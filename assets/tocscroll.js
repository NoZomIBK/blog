window.addEventListener('scroll', function() {
    if(pageYOffset > 520){
        document.getElementById("toc-container").setAttribute("state","lock");
    }
    else{
        document.getElementById("toc-container").setAttribute("state","");
    }
});