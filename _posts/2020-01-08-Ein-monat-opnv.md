---
layout: post
title:  "Ein Monat Öffentliche"
date:   2020-01-13
author: nozo
tags:   Blog Bus Bahn VRR Rant "Besser Leben"
image:  images/posts/2019-12-08-bus.jpg
---
Jetzt hab ich mal einen Monat [Bus- und Bahnfahren "genossen"]({% post_url 2019-12-08-OPNV %}), Zeit für ein Resümee.

Also zunächst mal zur App "Mutti" von Bogestra. Sie hat alle nötigen Funktionen und läuft auch ganz gut. Blöd ist nur das Konzept, dass die erste Fahrt bereits beim Kauf eines Zehnertickets
entwertet wird. Wenn man also eine Kurzstrecke fahren möchte, bei der das Ticket nur 20 Minuten gültig ist, muss man quasi an der Haltestelle sitzen und das Ticket kaufen, nicht zu früh aber noch 
bevor der Bus kommt, also wehe du hast gerade kein Internet oder sonst was!

Dann hatte ich ja noch in meinem vorherigen Post geschrieben, dass ich etwa die doppelte Fahrzeit hätte. Weit gefehlt. Inklusive Fußwege, Wartezeiten und Verspätungen wurde aus der 10-15
Minutenstrecke zum Einkaufen quasi grundsätzlich eine 45 Minuten Bummelei. Zum Trainieren und Einkaufen muss ich also eigentlich schon den kompletten Vormittag einplanen, anstatt nur zwei bis zwei
einhalb Stunden.({% post_url 2019-12-08-OPNV %})

Dazu kommt auch noch, dass es so ungemütlich ist, wie ich es aus Schul- und Studienzeit in Erinnerung hatte. Also unterm Strich ist es teurer, dauert länger, ist ungemütlicher und umständlicher als 
mit dem Auto zu fahren. Parkplatzprobleme kann man natürlich immer anführen, aber die habe ich normalerweise nie. Dort wo ich üblicherweise hin will gibt es genug Parkplätze oder Parkhäuser.

Aber irgendetwas gutes muss es doch bei der ganzen Sache geben! Wie vor einem Monat geschrieben, versuche ich durchaus ökologisch besser zu leben. Um die Gesellschaft in die bessere Richtung zu 
bekommen muss es Menschen geben, die ökologischer leben als der Durchschnitt. Also man hat definitiv was für das eigene Gewissen und die Moral getan. Aber als ich Mitte Dezember mal mit dem Auto 
fahren musste, habe ich einen weiteren Vorteil entdeckt: Es schont meine Nerven. Ich habe komplett vergessen, wie viele Idioten täglich auf der Straße unterwegs sind und wie unentspannt Autofahren 
ist. Ja, Bus und Bahn sind ungemütlich, dazu dann noch haufenweise Menschen, die quasi immer schwitzen, entweder weil es Sommer ist oder weil sie Winterklamotten tragen, der Bus aber auf 20° geheizt 
wird. Aber es ist tatsächlich immer noch weniger Nervenaufreibend als Autofahren.

Eine weitere spontane Erkenntnis war, das man auch einfach mal in die Stadt fahren konnte. Klingt verrückt, ich weiß. Wie bereits oben geschrieben, habe ich keine Parkplatzprobleme. Auch in Bochum
gibt es massenhaft Tiefgaragen, wo eigentlich immer Platz ist. Aber alleine schon am Straßenverkehr teilnehmen zu müssen und dann die Menschenmassen in der Innenstadt zu ertragen hat mir immer
gereicht, um nicht freiwillig in die Stadt zu fahren. Da der erste Stressfaktor wegfiel, war es viel einfacher die Motivation zu finden. Ich hatte auch den kleinen Anstoß, dass ich ein
Wichtelgeschenk für die fileee Weihnachtsfeier finden musste.

Ich bin auch tatsächlich mit dem Zug zur Weihnachtsfeier nach Münster gefahren. Aber wirklich oft werde ich das nicht machen. Die einzig finanziell sinnvolle Möglichkeit ist das SchönerTagTicket und
das gilt unter der Woche erst ab neun Uhr. Ich wäre also immer erst kurz vor elf im Büro und das ist eher unpraktikabel. Im Büro ist mir auch aufgefallen, dass die Feier für mich recht kurz wird,
weil meine letzte Verbindung um zehn Uhr fahren sollte. Im Nachhinein stellte sich heraus, dass nur der Bus bis vor meine Tür nicht mehr so spät fährt, bis nach Bochum wäre ich noch gekommen. 
Jedoch ist es ein ganzes Stück vom Bahnhof bis zu mir und Mittwochs fahren keine Nachtbusse. Also entweder hätte ich mein Auto am Bahnhof parken oder mir ein Taxi nehmen müssen, sofern die Mittwoch
Nachts verfügbar sein sollten, ich bin mir da nicht so sicher.

Und wo wir schon bei längeren (also weiter als 30min Busfahrt) Strecken sind, auch zu meiner Familie ging es mit der Bahn. Ganz angenehm ist, das Geldern mittlerweile auch zum VRR gehört. In meiner
Jugend war das noch anders und dadurch saß man einfach in dem Kaff fest. Die Preise finde ich zwar immer noch viel zu hoch und abschreckend aber für einmalige Fahrten ist es nicht so schlimm. Also
um es mal in Zahlen auszudrücken: Das VRR Tagesticket Preisstufe D kostet 29,90€ und Auto, inklusive Verschleiß, kostet mich ca 24€. Aber der Punkt ist eben, dass Bahnfahren unvermeidbar 
ungemütlich und zeitlich aufwändig ist, auch wenn es weniger aufregt als Auto zu fahren. Ich habe ja bereits genug gemeckert, dass wenigstens der Preis anlockend gestaltet werden sollte, am Rest
kann man wenig ändern.

Unterm Strich werde ich wohl dabei bleiben, in Bochum mit den öffentlichen Verkehrsmitteln zu fahren. Die Fahrten nach Münster werde ich wohl nur mit dem Auto machen können, bis sich preispolitisch
nichts ändert. Mal sehen, ob sich da irgendwann was tut.