---
layout: post
title:  "Animes im Frühjahr 2018"
date:   2018-07-10T19:05+0200
author: nozo
tags:   Anime Review
image:  images/posts/2018-07-10_teresa.jpg
toc:    true
---
Diese Season hab ich mir nen Haufen an Animes reingezogen. Allerdings sind auch ein paar Serien etwas auf der Strecke geblieben.

> warning "Spoilerwarnung"
> Ich geb zu den Animes meine Meinung ab und gegebenen Falls spreche ich da irgendwas spoilerndes an. Hiermit sei jeder davor gewarnt, lesen auf eigene Gefahr.

# Akkun to Kanojo
Fangen wir mit einem Kurzanime an. Zwei Minuten Unsinn pro Folge. Hauptthema ist die Beziehung zwischen zwei Schülern, wobei der Typ extrem tsundere ist. Dann noch ein wenig Slapstick Humor und fertig.
Ganz lustig, aber mehr als zwei Minuten Folgen würde ich den Humor nicht aushalten wollen.

# Tachibana-kan to Lie Angle
Ein Mädchen zieht in ein Wohnheim mit mehreren anderen Frauen, unter anderem einer Kindheitsfreundin. Viel sinnloser Humor und eine Schippe Ecchi, fertig ist auch hier ein unterhaltsamer Kurzanime.

# Isekai Izakaya: Koto Aitheria no Izakaya Nobu
Liebe geht bekanntlich durch den Magen. Zwar gab es jetzt bereits mehrere Animes über ein Restaurant, das in eine andere Welt verbunden wird und dort die Einheimischen mit wunderbaren Gerichten verblüfft.
Aber warum nicht ein weiteres Mal? Zudem hatte dieser Anime ein nettes Extra: Die Folgen wurden immer im Doppel, dafür im 14 Tage Rhytmus veröffentlicht. Nach der ersten Folge gab es immer das Hauptgericht
in echt mit Rezept nachgekocht und nach der zweiten Folge wurde ein Restaurant in Japan vorgestellt, das berühmt für das Hauptgericht ist.

# Saredo Tsumibito wa Ryuu to Odoru: Dances with the Dragons
Den am meisten vernachlässigten Anime der Season. Eine Fantasy-Welt mit Magie und Drachen. Es gibt dann noch irgendwas mit unterschiedlichen Reichen und scheinbar eine Verschwörung. Der Zeichenstil war
nicht überragend und auch so richtig gegriffen hat mich die Story nicht. Wahrscheinlich werd ich ihn Ende des Monats zu Ende gucken, wenn ich nen paar Tage im Schlaflabor verbringen muss.

# Toji no Miko
Eine Fortsetzung aus der letzten Season. Nachdem das ultimative Böse besiegt war, musste aufgeräumt werden. Und dann passiert das unfassbare: Das Böse scheint doch noch zu existieren und nun muss weiter
gekämpft werden und... An dieser Stelle wurde es dann ziemlich uninteressant und auch erstmal ignoriert. Da gab es interessantere Animes diese Season.

# Beatless
Auch hier eine Weiterführung der vorherigen Season. Jedoch entwickelt sich das Bild "Wer ist gut, Wer ist böse?" ein wenig und es wird mehr zur Ansichtssache, wer auf der richtigen Seite steht. Aber ein
weiteres Mal bin ich noch nicht zum Ende vorgestoßen.

# Caligula
Alternativwelten und Realitätsflucht, herrlich. Der Zeichenstil ist doch eher Gewöhnungssache, aber wenn man sich dran gewöhnt hat, dann ist der Anime doch recht interessant. Die Protagonisten sind in
einer virtuellen Welt gefangen, ohne dass sie den Grund dafür kennen. Nach und nach lüften sie die Hintergründe - jeder in dieser Welt hatte schlechte Erfahrungen in der realen Welt gemacht und sind in
diese Welt geflohen. Doch die Protagonisten finden, dass es besser ist das Leben in der echten Welt zu bestreiten, anstatt in einer künstlichen Welt zu leben. Die Story hat eine durchaus interessante
Entwicklung und es ist nicht alles vorhersehbar, aber richtig fesselnd war der Anime nun auch nicht.

# Dorei-ku The Animation
Hach ja, Herrschaft über andere - ob sie wollen oder nicht - immer wieder ein tolles Thema. Jedoch eine Zahnspange? Echt jetzt? Und dann gegeneinander spielen? Also da hätte man sich durchaus bessere
Sachen ausdenken können. Vor allem müssen beide Seiten zustimmen. Wieso sollte das irgendwer machen, wenn er ein gezinktes Spiel wittert? Und es taucht ein ominöser weiterer Spieler auf, der als einziges
ein bestimmtes Symbol hat - natürlich versucht man ihn wie jeden anderen Spieler zu gewinnen. Niemals kommt man auf die Idee, dass da etwas faul sein könnte. Nett finde ich die paar Szenen, wo die Sklaven
missbraucht werden. Es ist genau das, was realistisch wäre und sich jeder denkt, wenn man hört "andere versklaven". Man hätte die Szenen auch weglassen können, aber so kriegt es eine etwas abgedrehtere
Note. Aber ansonsten hat die Story so viele Löcher und Ungereimtheiten, dass ich regelmäßig Facepalmen wollte.

# Persona 5 the Animation
Ein Anime zu einem Spiel, dass ich nie gespielt habe. Ich weiß nicht, ob der Einstieg besser geklappt hätte, wenn man das Spiel kennt, aber ohne Vorwissen war der Anfang ziemlich voll von "What the Fuck".
Es gibt ein Metaverse, in das die Protagonisten wechseln können. Dieses wird durch das Unterbewusstsein der Menschen beeinflusst. Die ganz kaputten Menschen haben an bestimmten Punkten ihren Palast, je
nachdem gestaltet wie das Unterbewusstsein der Person die Welt um ihn herum wahrnimmt. Es beginnt beispielsweise mit der Schule des Protagonisten, das im Metaverse das Schloss eines Lehrers ist. Die Phantom
Thieves - wie sich die Protagonisten nennen - lernen sich in der neuen Welt umzusehen und erfahren, dass sie die verdrehten, egozentrischen Personen ändern können, indem sie den Schatz im Palast stehlen.
Daraus entwickelt sich ein Kampf für das Gute, was aber von der Polizei nicht so positiv gesehen wird. Erzählt wird alles in der Vergangenheit. Der Anführer der Phantom Thieves sitzt in einem Verhörraum
und wird immer mal wieder eingeblendet damit man nicht vergisst, dass die Geschehnisse in der Vergangenheit liegen. Zwar mag ich den Zeichenstil nicht besonders, aber die Story find ich durchaus interessant,
von daher verfolge ich diesen Anime auch gerne weiter, denn vorbei ist er offenkundig noch nicht. Ich weiß nur noch nicht, ob er in der Sommerseason weiter geht oder erst einmal Pause ist.

# Steins;Gate 0
Need a Mindfuck? Die Null am Ende des Titels hat mich dazu verleitet zu denken, es wäre die Vorgeschichte zu "Steins;Gate". In diesem ging es um Zeitreisen und deren Verwirrungen und Konsequenzen. Und
da haben sie nicht gespart. Auch die neue Serie hat reichlich Verwinklungen in der Erzählung und ich bin mir sicher, ich hätte die erste Serie vorab nochmal schauen sollen. Was mich tatsächlich stört
ist die Menge an Charakteren. Zwar haben sie eigentlich alle eine stark unterschiedliche Persönlichkeit, sodass sie grundsätzlich zu unterscheiden sind, aber für meinen Geschmack ist weniger manchmal
mehr. Schön finde ich den Aspekt, dass Dinge passieren, die in der anderen Serie in einer alternativen Zeitlinie passiert sind, jedoch leicht abgewandelt. Das Schicksal lässt sich also nicht so einfach
verändern.

# Shokugeki no Souma: San no Sara - Tootsuki Ressha Hen
Eine weitere Staffel des beliebten Kochanimes. Aber ganz ehrlich, man kann es übertreiben. Die Story war ja bereits vom Anfang an etwas abgehoben, aber so langsam wird es albern. Die Tochter des größten
Fleischproduzenten wird mal eben aus der Schule geschmissen? Als ob! Man lernt zwar weiterhin paar Hintergründe aus der kulinarischen Welt, zum Beispiel wo Kartoffeln üblicher sind als Reis, aber im Vergleich
zu den ersten Folgen kann man mit den vorgestellten Gerichten nichts anfangen. Was natürlich weiter vorhanden bleibt: Irgendwelche überraschende Wendungen, die Soma auftischt. Natürlich schlägt er mit
irgendwelchen ungewöhnlichen Mitteln hoch trainierte Köche in ihren eigenen Spezialgebieten, aber so muss das auch einfach sein, sonst wäre er ja kein außergewöhnlicher Held. Ja man merkt, ich werde müde
bezüglich dieses Animes, aber ich will wissen wie die Story weiter geht - richtig, sie ist immer noch nicht am Ende. Das entwickelt sich zu einem endlos Anime, i do not like that.

# Sword Art Online Alternative: Gun Gale Online
Full Dive Virtual Reality - ein Traum eines jeden Gamers. Es spielt im Universum von Sword Art Online, aber nicht mit den ursprünglichen Charaktären, warum auch nicht? Ich fand den ersten GGO Abstecher
mit Kirito ein wenig blöd - warum muss er wieder der Held sein? Nein, diesmal geht es um eine komplett andere Person, aber um noch irgendeinen Berührpunkt zu Sword Art Online zu schaffen, gibt es erneut
irgendeinen Hintergrund mit "im realen Leben sterben". Die Ingame-Freundin Pito der Protagonistin Llenn sucht Nervenkitzel und ist zerstört am Boden seit sie nicht an Sword Art Online mitmachen konnte
und erfahren hat, dass darin tatsächlich Leute gestorben sind. Also erzeugt sie ihr eigenes Deathgame indem sie beschließt sich umbringen zu wollen, sollte sie im zweiten Squad Jam getötet werden. Oder
keiner sie tötet. Außer Llenn, die im ersten Squad Jam überraschent den Sieg erringen konnte. Man hätte das nicht einbauen müssen. Es wirkt zu erzwungen. Aber ansonsten ist der Anime unterhaltsam und
es wird wenig drum herum erzählt sondern tatsächlich auf das Match fokusiert. Auch sehr interessant fand ich, dass der Gymnastikklub einer Highschool in der realen Welt als süße kleine Mädchen dargestellt
werden, aber im Spiel sind sie große, massive Söldnerinnen - trotzdem erkennt man die einzelnen Gesichter genau wieder.

# Hitori no Shita: The Outcast
Eine weitere Staffel des Kung-Fu Gedönsels. Es wurde aber mehr Hintergrund der gesamten Geschichte gebracht. Auf der anderen Seite hat Soran plötzlich so ziemlich gar nicht mehr kämpfen können. Das Tunier
wurde durch Betrügerreien gewonnen, warum? Er hatte doch eigentlich gezeigt, dass er kämpfen kann, also warum diese Farce? Auch waren so manche Entscheidungen im Anime nicht so ganz nachvollziehbar, hat
man da eventuell an der falschen Stelle gekürzt? Es war ganz amüsant, aber nicht herausragend.

# Fullmetal Panic! Invisible Victory
Fullmetal Panic. Als ich gesehen hab, dass eine weitere Staffel rauskommt, traute ich meinen Augen kaum. Nach so vielen Jahren wird die Story weiter geführt. Die alten Staffeln gehören zu meinen Lieblingen
und ich wurde bisher nicht enttäuscht! Der gesamte Stil, die Mischung aus Humor und Ernst, verzweifelte Situationen, die überwunden werden müssen, alles wie früher. Die Staffel geht in der nächsten Season
weiter, daher kann man noch nichts abschließendes sagen, aber bisher trifft es genau meinen Geschmack und wirft mich ein wenig in die Zeit der ersten Fullmetal Panic Staffeln zurück.

# Grancrest Senki
Der Rest der Serie. Die Entwicklung ist nicht sehr überraschend: Theo erkämpft sich einen Sieg nach dem anderen, Niederlagen können weggesteckt werden und am Ende siegt das Gute. Oder tut es das? Man
wird ja bereits relativ früh leicht angeteasert, dass die Magiergilde nicht ganz so sauber ist, aber jetzt wird das Ausmaß deutlich. Ein Orden mit einer Attentätergeheimeinheit ist nicht direkt das, was
man erwartet. Besonders interessant finde ich, dass die Magiergilde - sobald die Katze aus dem Sack ist - klar als das Böse dargestellt wird. Nachdem Theo jedoch gewonnen hat, erfährt man die Hintergründe
und merkt, dass die Gilde nicht nur sich selbst erhalten wollte, sondern sich tatsächlich für das Wohl der ganzen Welt eingesetzt hat. Es ist schön, wenn die Bösen nicht einfach nur Böse sind, sondern
aus ihrer Perspektive wirklich das Gute tun.

# Tada-kun wa Koi o Shinai
Eine Comedy-Romanze der alten Schule. Plötzlich taucht ein schönes Mädchen auf und der Protagonist verliebt sich in sie, ohne es zunächst zu merken. Irgendwann kommt eine schicksalhafte Wendung, die alles
gefährdet und dann schaffen sie es doch noch, das Schicksal zu wenden und zum Happy End zu gelangen. Aber es war eine schöne Variante dieses Standards. Viele kleinere Gags und Sidestories, die alles abgerundet
haben. Es war durchaus einer meiner Lieblingsanimes diese Season, weswegen Teresa es auch zum Titelbild dieses Beitrags geschafft hat.

# Amanchu! Advance
Zu Amanchu muss man nicht viel sagen. Amanchu! Advance war die Fortsetzung zu Amanchu, was ich erst später erfahren habe. Stören tut das nicht. Der Inhalt im Anime ist eher gering, aber er ist wunderschön
für nebenbei. Viele kurze und spaßige Geschichten unter Freunden. Eine kleine Moral hat das ganze am Ende aber doch noch: Statt sich bei Herausforderungen zu verkriechen, einfach mal versuchen, dann kann
man ganz neue Welten entdecken.

# Devils` Line
Was erwartet man von einem Anime mit Vampiren? Eigentlich nicht, dass ein junges, schönes Mädchen eine Narbe quer durchs Gesicht bekommt. Darüber hinaus ist es eine Mischung aus Intrigen und ein wenig
Brutalität. Aber der Hintergrund - die Vampire verlieren ihre Selbstbeherrschung und werden zu Monster, sobald sie nur ein wenig Blut sehen - ist so unglaublicher Humbug. Sie sollen angeblich komplett
in der Gesellschaft integriert sein und nicht auffallen, aber das triggert sie? Wie sollen die groß geworden sein? Mit anderen Kindern gespielt haben? Wie oft stolpert irgendwo jemand und schürft sich
ein Knie auf? Es würde einfach nicht funktionieren. Der Hintergrund, dass es Vampirhassende Gruppen gibt, die Menschen töten, um Hass gegen Vampire zu schüren, find ich jedoch voll passend. Könnte schon
fast ne Anekdote an reale Ereignisse sein. Unterm Strich war der Anime nicht herausragend. Die verwandelte Form der Vampire fand ich arg übertrieben und es waren mir mal wieder zu viele Ungereimtheiten.

# Otaku ni Koi wa Muzukashii
Otakus und Liebe, ein wunderbares Rezept für stumpfsinnige Witze. Und ja, genau das hat man bekommen. Klischees wurden ausgerollt, wo es nur ging. Auch dieses Mal muss ich sagen, der Zeichenstil war nicht
das Gelbe vom Ei, aber der banale Humor hat das mehr als ausgeglichen. Einer meiner Favoriten dieser Season!

# Comic Girls
Ein herrlich alberner Anime über Manga-Zeichnerinnen. Auch hier kann man nicht so viel sagen, außer es ist was zum Spaß haben. Das einzige Problem für mich war der unglaubliche Logikbruch: Eine Manga
Autorin, die eine Veröffentlichung gemacht hat und dabei nur negative Bewertungen bekommen hat, wird von ihrem Verlag in ein Mangazeichnerwohnheim gesteckt, damit sie sich verbessern kann. Als wenn irgendein
Verlag das machen würde! Nie im Leben. Vor allem nachdem sich nach Monaten nichts verbessert hat. Die sagen nichtmal Tschüss, während sie dich rausbitten.

# 3D Kanojo: Real Girl
Otakus und Liebe, ein wunder... moment, das hatten wir doch gerade schon! Ja auch hier ist das Thema wieder Otakus und ihre Probleme, jedoch auf einer ganz anderen Ebene. Wo "Otaku ni Koi wa Muzukashii"
einfach nur eine Otaku-Beziehung beschreibt und größtenteils humorvoll bleibt, zeigt "3D Kanojo" wesentlich ernstere Seiten. Zwar sind Tsutsuis soziale Fähigkeiten mehr als übertrieben schlecht, aber
so manche Szene spiegelt tatsächlich 1:1 wider, was sozial schwache Personen erleben können. Und ich hatte tatsächlich Momente, bei denen ich sagen musste "wer hat denen aus meinem Leben erzählt". Bei
einem Dialog bin ich mir sogar sicher, dass er nahezu wortgleich mit meiner Exfreundin stattgefunden hat. Ich konnte also tatsächlich die meiste Zeit mitfühlen und fand ihn durchgehend sehr interessant.
Leider hatte ich am Ende das Gefühl, dass zu viele Dinge offen geblieben sind. Und ein weiteres Mal bin ich kein Freund vom Zeichenstil.

# Darling in the Franxx
Um das direkt am Anfang zu klären: Der Zeichenstil... Aber dafür war die Story toll. Die Story ist zwar größtenteils vorhersehbar, aber es gibt immer wieder kleine Extras und Wendungen. Klar merkt man
schnell, dass die Erwachsenen nicht die Guten sind und die Klaxosaur nicht das absolut Böse, jedoch wird der wirkliche Feind erst sehr spät präsentiert. Das sorgt für eine Änderung in der gesamten Story
und erklärt auch ein paar Hintergründe. Leider sorgt es auch für das Ende von Hiro und Zero Two so wie es ist, was ich sehr schade fand. Auch fand ich die riesen Zero Two sehr seltsam, aber konnte man
drüber hinwegsehen. Wo wir gerade beim Ende sind: GEIL! So viele Animes lassen das Ende offen. Die Mainstory endet, aber dann wird man hängen gelassen und weiß nicht, wie es mit den Charaktären weiter
läuft. Ich hasse das, es fühlt sich so abrupt an. Nein dieses Ende ist ausgearbeitet. Es erzählt die Geschichte nach der Geschichte. Man wird nicht einfach fallen gelassen. Und es war ein herrliches Ende.
Manche Studios sollten sich hier mal ein Beispiel nehmen.

## Fazit
Die Season war vor allem zeichnerisch eher bescheiden. Und es gab sehr viele mittelmäßige Animes und nur wenige, wo ich richtig begeistert war und möglichst schnell die nächste Folge sehen wollte. Ich
hoffe der Sommer wird besser. Bisher sind es nicht so viele auf meiner Liste, aber Qualität geht über Quantität.