---
layout: post
title:  "Es ist still auf Twitch"
date:   2018-04-30T23:51+0200
author: nozo
tags:   Twitch Stream
image:  images/posts/2018-04-30_playing.png
---
Ja, ich weiß, ich habe schon lange nicht mehr gestreamt. Es ist bereits so lange her, dass es keine VODs davon mehr gibt. Aber das passiert natürlich nicht ohne Grund.

Zum einen musste ich zunächst das Streamen zurück schrauben, um mich ein wenig um meine [Gesundheit]({% post_url 2018-04-10-Zwanghafter-Sport-Motivation-ist-Mist %}) zu kümmern. Mit Arbeit und so weiter
frisst das leider unglaublich Zeit. Das wird auch so schnell nicht enden, daher muss ich immer gucken wie ich Zeit finde, aber Gesundheit geht vor.

Nach der Gesundheit kam aber noch etwas anderes dazu, nämlich das hier:

![Couch]({{ site.baseurl }}images/posts/752x423/2018-04-30_01.jpg)

Ja, es ist eine Couch. Wie kann mich eine Couch vom Streamen abhalten? Naja ganz einfach:

![Couch]({{ site.baseurl }}images/posts/752x423/2018-04-30_02.jpg)

Die Wohnzimmeraufteilung wurde ein wenig unpraktisch. Ich hatte keinen Platz mehr für meine Softboxen und daher habe ich beschlossen mit meinem gesamten Streamingkram in den kleinen Nebenraum umzuziehen.
In das Zimmer, in dem ich auch schon letztes Jahr zwischenzeitlich saß und gestreamt habe. Aber die Sache bleibt nicht ohne Hürden:

![Couch]({{ site.baseurl }}images/posts/752x423/2018-04-30_03.jpg)

Der Raum wurde zu meiner Abstellkammer und steht voll mit irgendwelchem Zeugs, was jetzt nirgends woanders mehr einen Platz hat. Zum wegschmeißen ist es auch zu schade oder noch zu brauchbar und die meisten
Sachen waren zu teuer um sie zu verschenken. Also muss ich erstmal Platz schaffen, um wieder streamen zu können. Das hatte ich auch schon geplant, dann sind aber andere Dinge wie Krankheit und Arbeit
dazwischen gekommen und ich habs auf ungewisse Zeit verschoben.

Jetzt könnte ich natürlich auch einfach ohne Softboxen und so nen Schnick-Schnack streamen, aber mein Streamingequipment habe ich bereits abgebaut:

![Couch]({{ site.baseurl }}images/posts/752x423/2018-04-30_04.jpg)

Und weil ich dick und ungelenkig bin, komme ich nicht mehr an alle nötigen Anschlüsse ordentlich dran, um es wieder im Wohnzimmer aufzubauen. Daher kann ich momentan nicht vom PC aus streamen.

Aber ich hab mir was besorgt:

![Couch]({{ site.baseurl }}images/posts/752x423/2018-04-30_05.jpg)

Einen Haufen Verlängerungskabel, damit ich im Wohnzimmer streamen kann, auch wenn der Streamingpc im Streamingraum steht (hoffentlich). Das ändert nichts daran, dass ich nicht an meinem PC streamen kann,
weil ich da weiterhin zu dick für bin, aber ich kann was anderes streamen:

![Couch]({{ site.baseurl }}images/posts/752x423/2018-04-30_06.jpg)

Ich habe eine Switch und werde sie benutzen!

Ja, ich hab mir was gegönnt. Ich hatte auch ein wenig ein schlechtes Gewissen als ich sie an einem Samstag bestellt hab. Montag Abend kam aber mein Vermieter mit einer netten Nebenkostenrückzahlung an
und die hat die Switch abgedeckt 💰.

Ja, ich werde das ganze noch testen müssen mit dem Aufbau, aber geplant ist am Freitag, den 4.5. ne Runde zu streamen. Da kommt Donkey Kong für die Switch raus und wenn da nichts schief geht, werd ich
das zocken. Ansonsten hab ich ja noch Zelda als Notfallplan.

Ich weiß noch keine Uhrzeit (irgendwann nach der Arbeit halt), aber am Freitag gibt es endlich mal wieder einen Stream auf [meinem Kanal](http://nozo.tv){:target="_blank"}.