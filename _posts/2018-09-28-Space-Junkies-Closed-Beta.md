---
layout: post
title:  "Space Junkies Closed Beta"
date:   2018-09-28T22:01+0200
author: nozo
tags:   Space-Junkies Ubisoft Gaming Beta Review
image:  images/posts/2018-09-28-Space-Junkies.jpg
---

Das neue Spiel von Ubisoft: [Space Junkies](https://spacejunkiesgame.com/) hat die Closed Beta erreicht. Ich war einer der glücklichen, die in die Beta gekommen sind... Wobei ganz ehrlich, ich glaube
es war nicht schwer in die Beta zu kommen. Und jeder Beta Teilnehmer hat noch drei Keys für Freunde bekommen, vielleicht hatten sie sogar einen zu geringen Andrang?

Leider ist es für VR Games relativ schwer, würde ich schätzen. Neben einen High End Rechner brauchst du die mindestens 400€ teure VR Hardware, um überhaupt bei VR Spielen mitmachen zu dürfen. Das schränkt
den Spielerpool schon massiv ein. Aber zu den generellen Problemen mit VR werde ich wohl ein ander mal schreiben.

Das Spiel selber ist relativ einfach erklärt: Weltraum + Cowboys. Bevor man voll durchstarten kann wird man ins Tutorial geschmissen. Die Grundlagen werden hier in einfacher Manier beigebracht. Was jedoch
außer acht gelassen wird: Man hat noch zwei Gadgets und kann wohin verschwindet überhaupt die Pistole, wenn man eine neue Waffe aufnimmt? Als meine Minigun leer war, habe ich die Pistole leider auf der
falschen Seite gesucht - dumm gelaufen. Sowas lernt man aber mit der Zeit, nur die Gadgets hätte man wenigstens erwähnen können. Genauso muss man die Menüführung durch Probieren kennen lernen und bis
ich wusste, wie ich aus der Lobby raus komme, hätte ich auch noch ein weiteres Match spielen können.

Das Spielen selber ist recht angenehm. Ich bin VR noch relativ wenig gewöhnt und mir wird durchaus gelegenlich unwohl bei den Bewegungen. Bei diesem Spiel hatte ich nahezu keine Probleme. Schlimm wurde
es nur, wenn ich steil nach unten geguckt habe und mich dabei irgendwohin bewegt habe, aber das kam nur selten vor. Ich weiß nicht genau, was es ist, aber Ubisoft hat es richtig gemacht! Das die Koordination
nicht optimal lief war auch nur Übungssache. Wenn ich paar Tage spielen würde, hätte ich auch weniger Probleme damit mich zu drehen und den Gegner dennoch zu treffen.

Jetzt kommen wir jedoch zu den negativen Aspekten. Zum einen sind die Waffen teilweise sehr unausgeglichen. Die Pumpgun beispielsweise ist sehr aufwendig - wie es sich gehört muss nach jedem Schuss einmal
durchgeladen werden - aber selbst auf geringer Distanz kein Oneshot. Die Slingshot andererseits muss auch zweihändig bedient werden, fühlt sich dabei aber einfacher zu händeln an und knallt echt hart
rein. Aber es ist ja erst die Beta, da wird Ubisoft noch dran drehen, wenn es tatsächlich ein Problem darstellt. Vielleicht bin ich auch einfach zu unfähig für die Pumpgun.

Ein größeres Problem sehe ich im Unterhaltungswert. Das Spiel ist lustig und actiongeladen. Die Musik macht Spaß und sorgt dafür, dass man auch nach einer verlorenen Runde am Ende ein wenig tanzt. Aber
nach nichtmal zehn Runden war das Spiel relativ ausgekostet. Ich habe bisher keine Informationen dazu gefunden, ob die Beta nur kleine Teile beleuchtet und das Endprodukt dann noch ein wenig mehr bietet
oder auch am Ende nur die Standard 2vs2 Spielmodi vorhanden sind. Wenn das wirklich der gesamte Inhalt ist, wird das Spiel nicht überleben. Man braucht eine gewisse Playerbase um Multiplayer am leben
zu erhalten und damit tun sich VR Spiele grundsätzlich schwer. Sollte der Großteil der Spieler so wie ich in dem Spiel eher eine kurze zwischendurch Beschäftigung sehen, wird man für die wenigen Partien
keine Mitspieler finden und daher das Game beiseite legen. Es wäre eigentlich schade drum, weil Potential hat das Spiel. Nur wenn man es nicht früh genug entfaltet wird es sterben bevor es richtig leben
konnte.

Soweit meine Meinung von Space Junkies, ich bin gespannt was draus wird.