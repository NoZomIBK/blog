---
layout: post
title:  "Zeitreisen sind möglich!"
date:   2018-04-25T17:57+0200
author: nozo
tags:   Rant Politik Philosophie Alltäglich Satire
image:  images/posts/2018-04-25-zeitreise.jpg
---
Wie viele Jahre schon fantasiert man über Zeitreisen? Wie viele Theorien wurden entworfen, verworfen und wieder ausgegraben, ob Zeitreisen möglich sind und wenn ja, wie und welche Folgen hätte es?
Bislang funktionierte es nur in eine Richtung mit der Reise durch die Zeit. Die Geschwindigkeit dieser Zeitreise lies sich zwar variieren, entweder viel zu schnell - während man Urlaub hatte und viele
tolle Sachen erlebt - oder schleppend langsam - während man im Regen auf den nächsten Bus wartet und nicht mal Musik mit hat - jedoch ging es stehts nur in Richtung Zukunft.

Doch nun ist es endlich gelungen, die ganze Welt auf den Kopf zu stellen. Doktor Barkus Schnöder von der Chronisch Senilen Universität ist einer der Leitenden Forscher des Institute for Direct
Inversion Of Time (IDIOT) und feiert diesen Augenblick mit den Worten: "Niemand wird mehr uralte, unleserliche Schriften entziffern müssen, um die Geschichte zu verstehen. In wenigen Jahren können wir
die Kreuzzüge nahezu miterleben".

Die bisherigen Erfolge belaufen sich auf die Zeit des Aufstiegs der NSDAP vor dem Zweiten Weltkrieg. Laut Schnöder ist dies ein europaweites Projekt mit dem man reproduzieren will, wie es zur Machtergreifung
der Nationalsozialisten kam. Hier baut man stark auf die faktisch exakte und stets objektiv arbeitende Wissenschaftszeitschrift "Bild". Aber auch moderne Technik kommt zum Einsatz, so hat man in
Kooperation mit Facebook ein neues System etabliert, welches unter Kennern als "Hetzpost" bekannt und beliebt ist.

In dem neusten Versuchsaufbau - der am 1. Juni 2018 starten soll - will man nun versuchen die Grenze noch ein wenig weiter zu verschieben und in eine Zeit sehen, bevor die Säkularisierung begann um
sich zu greifen und den Menschen jegliche moralische Orientierung nahm. Wie lange dieses Experiment dauern wird, konnte Schnöder nicht beantworten, aber man werde versuchen sich bis ins Mittelalter
vor zu tasten. Ob es soweit kommt weiß nur Gott. Und der läuft momentan eher schreiend, mit den Händen fuchtelnd durch den Himmel, weil selbst er kann sich denn Schwachsinn auf Erden nicht länger antun.

/Satire

Und nun mal im Ernst. Bayern war ja schon immer ein wenig komisch, aber im Bullshit übertreffen sie sich immer wieder.

https://twitter.com/Markus_Soeder/status/988768341820170240
