---
layout: post
title:  "Mexikanische Rezeptsammlung"
date:   2019-06-02T13:48+0200
author: nozo
tags:   Rezepte Kochen Küche
image:  images/posts/2019-06-02_burrito.jpg
toc:    true
---
Heute gibt es mal eine kleine Sammlung an mexikanischen (mehr oder weniger authentisch) Grundlagenrezepten, also Saucen, Beilage, Gewürzmischung. Kombiniert hat man dann beispielsweise Burritos.

# Gewürzmischung
Fangen wir mal mit meiner üblichen Gewürzmischung für Hackfleisch an. Man kann es natürlich auch für Hähnchen oder sonstwas nehmen, oder es für Chili con Carne verwenden.

## Zutaten
- 2-4 EL Chilipulver (je Schärfegrad/Geschmack)
- 1 TL Cayenne Pfeffer
- 1 TL Salz
- 1 TL Knoblauchgranulat
- 1 TL Zwiebelgranulat
- 1 TL Paprikapulver
- 1 TL Oregano
- 1 TL gemahlener Kreuzkümmel
- 1 TL Koriander
- 1 TL Pfeffer
- 2 EL Stärke

## Zubereitung
Mischen? Das ist ne Gewürzmischung, nichts besonderes. Na gut, ganz richtig ist die Aussage nun nicht, weil ein wenig mehr sollte man schon noch sagen: Die Menge reicht bei mir etwa für ein Kilogramm
Hackfleisch. Außer für Feiern wird man selten so viel brauchen, daher pack ich das immer in eine Dose, dann brauch ich nie Knorr fix. Man sollte es auch mit ein wenig kaltem Wasser anrühren, bevor man
es zum Fleisch gibt, dann bekommt man eine schön saftige Fleischpampe, die man wunderbar in Burritos packen kann. Wenn man das nicht möchte, sollte man natürlich die Stärke weglassen, aber ich
empfehle es.

# Tomaten Salsa
Oftmals einfach nur als "Salsa" bezeichnet, aber an sich heißt das nur Sauce und Saucen gibt es viele.
Diese Salsa ist wunderbar für Nachos, schmeckt deutlich frischer als die aus dem Supermarkt und kann auch für Wraps oder ähnliches super benutzt werden.

## Zutaten
- 800g Gewürfelte Tomaten (ich nehm die aus der Dose, man kann aber auch selber würfeln)
- Große Zwiebel
- 4-6 EL (Soja) Öl
- Grüne Paprika
- 6 EL Brauner Zucker
- 1-2 Chilischoten (Habanero)
- 2 EL Stärke
- 1 TL Salz
- 1/2 TL Pfeffer
- 1 TL Koriander
- 1 Knoblauchzehe
- Saft einer Limette

## Zubereitung
Ja es ist eine riesige Menge dafür, dass man es als Nachodip nutzen soll, aber so passt es am besten von den Verhältnissen mit Paprika, Tomate und Zwiebel. Ich mache mir immer mehrere Einmachgläser
voll.
Die Chilischoten, hälfte der Paprika und hälfte der Zwiebel (wirklich große Zwiebel, fast Handball groß) häcksel ich klein, bis es eher Püree ist. Die zweiten Hälften von Zwiebel und Paprika werden
dann nur noch fein gehäckselt, dass es fein gewürfelt ist. Man kann das natürlich auch von Hand machen, wenn man ansonsten den Tag nichts mehr vor hat. Der gewürfelte Anteil kommt mit dem Öl in den
Topf und wird angebraten, anschließend kommt das Chili-Paprika-Zwiebel Püree und die Tomaten rein. Ab hier sollte man sehr viel Ausdauer oder einen Sklaven haben, weil es muss viel gerührt werden. Ich
habe glücklicherweise ein Rührgerät:

https://www.instagram.com/p/ByNMbzUinxn/

Der ganze Rest an Zutaten wird nun in einer Schüssel zusammengemischt und dann mit in den Topf gekippt. Weil es primär Nachodip sein soll, dürfen natürlich keine Vitamine über bleiben. Deswegen muss
das alles nun eine Stunde köcheln (habe ich schon den Sklaven erwähnt?). Es ist tatsächlich für den Geschmack nötig, dass es so lange köchelt, ich habe mit 15 Minuten angefangen, aber das beste
Ergebnis gab es erst nach einer Stunde.
Anschließend in Einmachgläser füllen und abkühlen lassen.

# Chipotle Salsa
Eine leckere Sauce, die ich in erster Linie für Burritos nutze.

## Zutaten
- 1 Becher Saure Sahne oder Schmand (je nach gewünschter Konsistenz)
- 1-2 Chipotle Pepperoni in Adobo Sauce

## Zubereitung
Häcksel die Pepperoni bis zur Unkenntlichkeit (je nach Häcksler hilft ein wenig von der Sauren Sahne/Schmand) und vermisch es mit dem Molkereiprodukt deiner Wahl. Ich mach mir auch gerne einen Klecks
der Sauce in mein Chili oder auf Wraps mit Salat.

# Roter Reis
Nicht einfach nur Reis, wäre ja langweilig.

## Zutaten
- Reis
- Tomatenmark
- Paprikapulver

## Zubereitung
Von den Mengen habe ich leider keine Ahnung, ich mach das immer nach Gefühl. Man kocht den Reis wie man es für gewöhnlich täte, rührt aber Tomatenmark und Paprikapulver mit ins Wasser. Das geht
natürlich nicht so gut, wenn man Beutelreis benutzt. Es zieht nicht immer alles in den Reis, weswegen es sich schonmal Reste am Boden bilden können, da habe ich leider noch keine Lösung für gefunden.

# Pico de Gallo
Manche nennen es Salsa, andere Salat, ich finde es ist etwas dazwischen. Ich würde es nicht so essen, wie einen Salat, aber es ist nicht saucig genug um eine Sauce zu sein. Nennen wir es eine
Burritokomponente.

## Zutaten
- 6 Roma Tomaten
- 1/2 Rote Zwiebel
- Frischer Koriander
- 1 Jalapeno
- Saft einer halben Limette
- 1 Knoblauchzehe
- 1 Prise Kreuzkümmel
- Salz
- Pfeffer

## Zubereitung
Die Tomaten müssen entfleischt, sonst wird es doch eine Sauce, und fein gewürfelt werden. Ebenso die halbe Zwiebel, Jalapeno (entkernt) und Knoblauchzehe fein würfeln. Den Koriander hacken, dass man
etwa eine Menge von drei Teelöffeln hat, wobei Koriander sehr geschmacksfrage ist und man da testen sollte, wie viel man mag. Anschließend alles gut vermengen und für ein bis zwei Stunden im
Kühlschrank ziehen lassen.

Lasst es euch schmecken.