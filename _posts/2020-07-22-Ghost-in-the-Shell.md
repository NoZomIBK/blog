---
layout: post
title:  "Ghost in the Shell"
date:   2020-07-22T20:30+0200
author: nozo
tags:   Review Cyberpunk Film Rant
image:  images/posts/2020-07-22-gits.jpg
---
Ich steh auf Cyberpunk. Und Action. Und gute Effekte. Da dachte ich mir, ich könnte mir mal wieder Ghost in the Shell angucken. Ja, die Hollywood Verunstaltung.
Ich hatte ihn als "Ja, es ist eine Schande für das Originalwerk, aber technisch okay" in Erinnerung, aber die ersten paar Minuten sorgen schon dafür, dass ich
diesen toten Blog wieder zum Leben erwecke, so sehr kotzt es mich an.

#### Philosophie
So ziemlich das erste, was in dem Film gemacht wird: Die komplette philosophische Auseinandersetzung mit "was ist die 'Shell' und was ist der 'Ghost' in dieser?"
wird in den Müll getreten. Die Shell ist dein Körper und der Ghost ist deine Seele. Punkt. Man kann nicht mal mehr drüber nachdenken, es wird direkt in Beton
gegossen. Könnte die Shell eventuell auch äußere Gegebenheiten sein? Die Gesellschaft? Lebensumstände? Ist der Ghost dein Bewusstsein? Dein Charakter? Deine
Erinnerungen? Was ist, wenn du ein Bewusstsein hast, aber keinen Körper? Und wenn der Ghost in der Shell sitzt, wie wird er von der Shell beeinflusst? Geformt?
All diese Überlegungen werden über Bord geworfen, damit auch der dümmste Kinobesucher erfährt, was es mit 'Ghost' und 'Shell' auf sich zu haben hat.  
Ebenso die offene Frage, auf die der Originalfilm hinausläuft - ist ein künstliches Bewusstsein echtes Leben und schützenswert - interessiert hier nicht mehr 
wirklich. Ein Bewusstsein aus den Informationen im Netzwerk als Antagonist, das letztendlich Asyl sucht, weil es ansonsten gelöscht, beziehungsweise getötet
würde, das ist Hollywood zu hoch und man möchte lieber klassische und eindeutige Bösewichte präsentieren.

Philosophie? Brauchen wir nicht!

#### Whitewashing
Ein elendiges Thema. Als die Trailer zum Film erschienen, hatte ich das bereits mit einem Kollegen. Ich hatte damals die Problematik "Whitewashing" noch gar nicht
gekannt. Und nach den Trailern hatte ich auch hier kein wirkliches Problem gesehen, mit dem Hintergrund, dass ich die Animes kannte. In diesem waren nahezu alle, 
die man auch in dem Trailer gesehen hat, fast vollvercybert waren. Dazu kommt, dass Japan sich bei Schönheitsidealen stark an westliche, beziehungsweise kaukasische
Standards orientiert. Daraus wäre es eine plausible Erklärung, dass die künstlichen Körper nicht asiatisch aussehen. Aber weil der Film auf das Original einen
Dreck gibt, war meine Annahme falsch. Bis auf Major Kusanagi gibt es keine Vollcyber. Und einen eigentlich japanischen Charakter besetzen wir dann noch mit einem
schwarzen Schauspieler, weil so viel Diversität muss schon sein, wenn wir Whitewashing betreiben.  
Okay, mit der Aussage tue ich dem Film ein wenig Unrecht. Leider wirklich nur wenig. Togusa und Saito werden von Asiaten dargestellt, jedoch ist ihre Bedeutung im
Film eher gering. Aramaki ist die größte asiatisch besetzte Rolle. Batou ist in meiner Sicht das größte Problem. Im Original ist er Japaner und im Gegensatz zu
Major Kusanagi ist es kein komplett künstlicher Körper. Würde er von einem Japaner gespielt werden, hätte ich tatsächlich kein Problem mehr damit, dass die
Hauptrolle nicht asiatisch besetzt wurde, vor allem da am Ende durch die echte Mutter von Motoko deutlich gemacht wurde, dass sie eigentlich Japanerin ist, jedoch
in einem falschen Körper lebt.

#### Bösewicht
Die Zerstörung der Philosophie hinter dem Original kann natürlich nicht vollendet werden, ohne etwas am Bösewicht zu drehen. Und wie kann man einen Bösewicht
besser einführen als mit den Worten "Sie ist eine Waffe und die Revolution meines Unternehmens"? Und anschließend noch eine Kamerafahrt durch die futuristische
Stadt, in der überall virtuelle Werbung aufleuchtet, aber die größten und protzigsten Einblendungen kommen von seinem Unternehmen: Hanka. Offensichtlich ein Global
Player mit viel Macht. Und ein machthungriger Konzernchef will noch mehr Macht. Und das er die Macht hat, wird auch deutlich mit dem Satz "Sie wird sich Sektion 9
anschließen, sobald sie einsatzbereit ist". Er, als Konzernchef, entscheidet darüber, wer in die Spezialeinheit der Sicherheitsbehörden kommt. Das ist definitiv
sehr cyberpunkig, auch in Shadowrun wird ein Großteil der Polizeiarbeit von Konzernsicherheit erledigt, inklusive einhergehender Privilegien, aber es weicht
komplett vom Original ab, nur um diesen Bösewicht einzubauen. Im Anime gibt es Hanka auch, aber die Firma hat eher die Rolle eines Zulieferer und Wartungspartner
der staatlichen Polizei.  
Kurz darauf wird ein führender Wissenschaftler des bösen, machthungrigen Konzerns angegriffen und man bekommt den offensichtlich Bösen gezeigt. Zwei Bösewichte?
Das kann doch nicht sein! Natürlich nicht, das würde doch zu komplex werden, für den einfachen Zuschauer. Daher wird jetzt den Film über Hideo Kuze, der
offensichtlich Böse, gejagt, bis der nahezu offensichtliche Twist kommt, dass er nur ein Opfer des wahren Bösewichts - der machthungrige Konzernchef - und auf
Rache und Gerechtigkeit aus ist. Um zu unterstreichen, dass er eigentlich nicht so böse ist, ist er derjenige, der Major Kusanagi die Wahrheit offenbart, dass
auch sie von Hanka hintergangen wurde.

#### Background Stories
Wie bereits beim Whitewashing angesprochen, die Hintergründe der Charaktere sind komplett ignoriert und neu aufgebaut worden. Einfach, damit es in die Story passt.
So heißt Major Motoko Kusanagi im Film auch Mira Killian. Sonst würde das nicht mit dem versauten Plot zusammen passen. Auch wie sie zum Vollcyber geworden ist,
wird für den Film frei erfunden. Im Original Film wird es gar nicht thematisiert, jedoch in der Serie wird herausgestellt, dass sie quasi von Kindheit an in einem
künstlichen Körper lebt, was sie zu einer Besonderheit macht.  
Auch hat sie zu den anderen Personen in ihrer Truppe ein anderes Verhältnis. Mit Batou hat sie in Südamerika gekämpft. Saito hat sie erst als Feind gegenüber
gestanden. Das bringt komplett andere Grundlagen mit sich als im Film, in dem sie vor nicht mal einem Jahr in die Truppe geschmissen wurde und Batou nicht ihr
langjähriger Vertrauter ist.

#### Und alles in den Mixer!
Der Anime aus dem Jahr 1995 war relativ slow paced und hatte nicht viel Material für einen kompletten Hollywood Blockbuster. Da kann man natürlich verstehen,
dass man irgendwo zusätzliches Material herholen muss. Man nimmt also den Originalfilm, schmeißt ein wenig was aus den Serien mit in den Mixer und klebt das
alles mit abgedroschenen Unsinn in die klassische Form der üblichen Hollywoodplots.  
Hideo Kuze, der rachsüchtige Bösewichtopferterrorist, handelt mit mitteln des Puppeteers - dem Antagonisten aus dem Original - kommt aber aus einem komplett
anderen Kontext. Auch die Beweggründe sind komplett anders. Die Verbindung zwischen Kuze und Kusanagi ist ähnlich gehalten, aber alles wurde für den Plot zurecht
gestückelt. Anstatt freiwillig voll vercybert zu werden, sind sie Experimente des machthungrigen, bösen Konzernchefs. Irgendwoher muss man ja einen Bösewicht
holen.

#### Wo ist das Positive
Irgendwo muss es doch nun etwas positives geben. Wie schon ganz am Anfang gesagt, die Produktion ist definitiv gut. Der Film bringt hervorragend den Cyberpunkflair
rüber. Die Effekte sind beeindruckend. Und auch wenn sie den kompletten Plot durch den Fleischwolf gedreht haben, wurden viele Szenen aus dem Original übernommen
und eingebaut. Sowohl die Sequenz vom Zusammensetzen des Cyberkörpers, was auch in das Musikvideo von King of my Castle - einer meiner allzeit Lieblingslieder - 
übernommen wurde, als auch der Sprung vom Hochhaus und die Verfolgung über den Markt mit dem Kampf im Wasser. Besonders beeindruckend übernommen ist auch der
komplette finale Kampf mit dem Panzer, bei dem Major Kusanagi ihren Cyberkörper komplett überlastet. Ich hatte das Gefühl, man könnte beide Filme übereinander 
legen und man sieht das gleiche, die platzenden Gelenke, die reißenden Synthetikmuskeln. So sehr mich die Plotverwurstung ankotzt, diese Szenen bringen mir
Gänsehaut. Vielleicht ist das aber auch der Grund, warum ich die Verfilmung so versaut finde. Das Original mitsamt den Serien waren offenkundig bekannt. Und
trotzdem wird eine Hollywood Storyline da drüber gepresst.

Dazu kommt noch die Darstellung der Charaktere. Jedes Mitglied in der Truppe, das nicht nur als kleiner Seitenauftritt erscheint, ist deutlich am Original
gespielt und man kann wunderbar die wichtigen Charakterzüge sehen. Major Kusanagi geht Risiken ein, die sie selbst gefährden, um ungeklärten Fragen auf den Grund
zu gehen und missachtet dabei durchaus auch ihre Anweisungen. Batou ihr engster Vertrauter und steht ihr immer zur Seite. Er ist eher der Mann fürs Grobe, aber
offensichtlich ist sein Herz weich genug, um sich um die Hunde zu kümmern. Togusa hält sich deutlich stärker an die Regeln und ist eher der ruhige Charakter.
Und zu guter letzt Aramaki, der Chef der Einheit. Er macht deutlich, dass er voll hinter seinen Leuten steht. Er scheint sich zwar üblicherweise den Mauscheleien
in Politik und Industrie unterzuordnen und achtet darauf, dass die Vorschriften eingehalten werden, macht aber deutlich, wo seine Loyalität liegt und wenn es hart
auf hart kommt, steht seine Truppe für ihn an erster Stelle.

---
Unterm Strich kann man sagen, dass dieser Film vom Aussehen her hervorragend, im Inhalt aber schlechter Abklatsch des Originals ist. Und wenn man Angst hat, dass
der Film nicht gut ankommt, wenn die Hauptrolle nicht Scarlett Johansson, sondern eine Japanerin spielt, dann sollte man sich eher Gedanken um die Qualität des
Films machen, nicht um das Casting, weil dann fehlt dem Film scheinbar was. Wie wäre es mit etwas mehr Philosophie und etwas weniger generischem Plot?