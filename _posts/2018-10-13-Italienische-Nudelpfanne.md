---
layout: post
title:  "Italienische Nudelpfanne"
date:   2018-10-13T11:58+0200
author: nozo
tags:   Rezepte Kochen Küche
image:  images/posts/2018-10-13_italienische-nudelpfanne.png
---
Ich dachte mir, heute ist ein fauler Samstag (obwohl ich nachher Feuerwerk machen muss), also stelle ich mal ein faules Rezept in die Welt.

## Zutaten
- Nudeln
- Gewürzte Italienische Tiefkühlgemüsemischung
- Ein Becher Schmand
- Salz und Pfeffer

## Zubereitung
Die Nudeln kocht man ganz wie gewohnt. Das Tiefkühlgemüse schmeißt man in eine Pfanne. Wenn man eine Pfanne mit Deckel hat gibt man etwa 100ml, ohne Deckel 150-200ml Wasser dazu und bringt es zum 
kochen. Wenn das Gemüse nahezu gar ist, gibt man den Schmand dazu und weil dieser die Würze vom Tiefkühlgemüse ein wenig ausgleicht, noch ein wenig Salz und Pfeffer zum nachwürzen. Das Ganze einfach 
noch ein klein wenig einkochen lassen, dass es eine cremige Konsistenz bekommt und fertig.

## Mein Fazit
Es ist unglaublich wenig aufwendig und man muss nur ab und an umrühren. Dafür schmeckt es super. Es ist also großartig dafür geeignet, wenn man nach dem Sport was essen möchte, aber keine große Lust 
hat aufwendig etwas zu kochen.

👨‍🍳 Super simpel  
🍳🍳🍳🍳 echt lecker