---
layout: post
title:  "OBS Stinger und die Odysee der Dateiformate"
date:   2018-10-19T15:46+0200
author: nozo
tags:   Twitch Stream Rant Photoshop OBS
image:  images/posts/2018-10-19-OtakuAreOver.jpg
---
Ich bin mal wieder dabei gewesen, für meinen Stream zu basteln. Dieses Mal wollte ich eine Stinger-Transition machen, also einen Szenenwechsel mit einer Animation drüber. Nur war das ganze nicht ganz 
so einfach.

Um die Frage, wie man eine Transition in Photoshop macht, vorab abzuhandeln: 

1. Lernt, wie man eine Animation in Photoshop macht
2. Startet und endet die Animation mit einer komplett transparenten Arbeitsfläche
3. Irgendwo in der Animation braucht ihr einen Frame (besser mehr), der alles abdeckt, damit dahinter der Szenenwechsel stattfinden kann

![](/images/posts/2018-10-19/size.png){: .align-right}
Damit hat man schonmal die Animation. Das Problem ist, egal wie man sie ausgibt, sie wird extrem viel zu groß. Meine Animation ist 3 Sekunden lang, zwar mit 60 FPS, aber dennoch sind 1.4 GB 
übertrieben groß. Zugegeben, das ist die Version ohne Komprimierung, aber auch mit guter oder mittlerer Qualität lagen die Dateien bei 800 MB. Das ist natürlich zu viel für OBS, das bei jedem Übergang
versucht hat, den Übergang laufen zu lassen und ihn natürlich nicht ruckelfrei abspielen konnte.

Aber es gibt ein Dateiformat, dass extra für Web existiert und Transparenz unterstützt: WebM. Jetzt musste ich nur noch irgendwie meine .mov Datei in eine .webm konvertieren. Die Suche nach einem 
Programm erwies sich jedoch schwierig. Entweder gibt es nur eine Testversion mit Wasserzeichen oder der Konverter unterstützt kein WebM beziehungsweise keine Transparenz.

Aber ich bin ja ein Linux Kind und unter Linux gibt das ultimative Konvertierungstool: ffmpeg. Wenn es doch nur so einfach wäre. Ich habe diverse Einstellungen probiert, aber entweder ist die 
Transparenz flöten gegangen oder die Qualität, dann sah es nur noch wie so ein Pixelbrei aus.

![](/images/posts/2018-10-19/quality.png){: .align-left}

Die Suche nach einem brauchbaren Konverter und die Versuche mit ffmpeg hatten mittlerweile den halben Tag gedauert und ich begann so langsam zu zweifeln. Irgendwie musste das gehen, immerhin gab es 
das ja auch in anderen Streams und die können auch nicht mal eben eine 1 GB Datei dafür nutzen. Das Netz spuckte aber extrem wenige Informationen dazu aus und eine teure Videosoftware wollte ich mir 
jetzt nicht dafür besorgen.

Irgendwann bin ich aber endlich über die Lösung gestolpert - in [diesem Video](https://www.youtube.com/watch?v=zMTD7FnZUwI). Des Rätsels Lösung ist WinFF - eine Oberfläche für ffmpeg. Die Entwickler 
scheinen aber nicht mehr so ganz aktiv zu sein, weil einen einfachen Installer auf deren Seite konnte ich nicht finden, nur auf diversen Freeware Seiten, die gerne mal Werbung oder sonst irgendeinen 
Mist mitliefern. Daher passt auf, wo ihr euch das Ding ladet. Aber wie erwähnt, es nutzt im Hintergrund ffmpeg und man bekommt auch die Kommandozeile ausgegeben, die eine Animation mit brauchbarer 
Qualität, inklusive Transparenz und einer benutzbaren Große von nur 1,4 MB herausbringt:

`ffmpeg.exe -y -i transition.mov -f webm -aspect 16:9 -vcodec libvpx -g 120 -level 216 -profile:v 0 -qmax 42 -qmin 10 -rc_buf_aggressivity 0.95 -vb 2M -acodec libvorbis -aq 90 -ac 2 transition.webm`

Damit hab ich nun endlich das Ergebnis, das ich haben wollte. Ich fand es etwas ärgerlich, dass die Suche so umständlich und lange war und ich dadurch kurz davor war das Handtuch zu werfen. Aber das 
hat sich erledigt und nun kann ich mich auf den Rest der Vorbereitung für den Stream machen. Und irgendwann - im November wahrscheinlich - geht die Sommerpause zu Ende.