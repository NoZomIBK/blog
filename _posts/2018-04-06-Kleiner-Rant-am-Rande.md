---
layout:      post
title:       "Der Rant über die Ungenauigkeit"
date:        06.04.2018T19:11+0200
author:      nozo
tags:        Backen Kochen Küche Rant
image:       images/posts/rage.jpg
description: Nur ein ganz kleiner Rant am Rande
---
Nur ein ganz kleiner Rant am Rande. Kochen ist nicht meine Leidenschaft, Backen schon eher, aber bei beidem stolper ich gerne mal über Rezepte, bei denen der Verfasser offensichtlich aus einem kleinen
Dorf kommt, in dem es nur einen kleinen Laden gibt, wo jedes Produkt nur einmal vorhanden ist.

Es gibt einfach *keinen* anderen Weg, wie diese Personen mit Angaben wie "ein Becher", "eine halbe Tafel" oder "zwei Packungen" glauben eine brauchbare Mengenangabe zu machen! Was für ein Becher? Den
mit 200 oder 150ml? Und die Tafel Schokolade, eine 100g, 200g, 300g? Und welche Menge hat deine Packung, lieber Rezepteauthor? Bei mir im Laden habe ich fünf verschiedene gefunden! Und bei "eine Tasse
Zucker" macht es zwischen meinen kleinen und großen Tassen auch ein Verhältnis von 300% aus.

*Verfickte Scheiße nochmal*, wie soll man so ein Rezept nachmachen? Klar kann man experimentieren, aber ich will keine fünf Torten versuchen zu backen um die richtigen Angaben herauszufinden. Wir leben
im Jahre 2018, jede normale Küche enthält Messbecher und Waagen. Wenn ihr nen Rezept macht, dann arbeitet mit brauchbaren Angaben!