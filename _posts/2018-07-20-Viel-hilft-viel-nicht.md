---
layout: post
title:  "Viel hilft viel... Nicht!"
date:   2018-07-20T23:32+0200
author: nozo
tags:   Rant
image:  images/posts/dislike.jpg
---

Ich weiß, die ganzen Weltverbesserer meinen es gut. Man versucht die Menschheit über die bösen Machenschaften der Politiker und Lobbies aufzuklären oder sich wenigstens für die Schwachen stark zu machen.
Wenn ich mit Change.org Petitionen überhäuft werde, oder in meiner Timeline von einer Person fünf verschiedene Petitionen zu ein und derselben Sache - nur auf jeweils einer anderen Petitionsplattform -
gespült bekomme, dann sorgt es nur für eins: Ich ignoriere es. "Hilf XY bei...", "Unterschreibe jetzt für...", "Nur noch BLA Unterschriften, um...". Es ist löblich, dass ihr die Welt verbessern wollt,
aber so entsteht nur Desinteresse.

![]({{ site.baseurl }}images/posts/2018-07-20_postings.png)

Ähnlich verhält es sich mit den "Teile dies, wenn..."-Postings. Ich teile schon so keine Spruchweisheiten, aber sobald diese indirekten Aufforderungen mit passiver Aggressivität eingebaut wird, dann stellt
sich bei mir das Verlangen ein, dagegen zu wirken. Soso, 97% teilen es eh nicht? Na dann wollen wir doch mal versuchen auf 98% zu kommen. Allein schon diese aus den Fingern gezogenen Prozentangaben. "Ich
hab den Mut das zu posten" jaaa, es ist so unglaublich mutig nen fucking Text als Bild vom Sofa aus auf Facebook zu stellen! Leute, holt die Medallien raus, wir haben hier einen ganz tapferen Helden!

Bei den ganzen Sachen rede ich ja nichtmal von "@S muss dir eine Frikadelle in den Ausschnitt stopfen" Bullshittereien. Es sind durchaus Posts, die zu sozialem Engagement aufrufen oder über Missstände
aufklären wollen. Aber der Weg ist einfach scheiße!
