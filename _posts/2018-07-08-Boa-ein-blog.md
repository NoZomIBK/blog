---
layout: post
title:  "Boa, ein Blog"
date:   2018-07-08T14:21+0200
author: nozo
tags:   Blog
image:  images/posts/2018-07-08_abandoned-office.jpg
---
Ja, es ist ein wenig Zeit vergangen, seit meinem letzten Post, welcher auch darüber ist, dass ich Projekte schleifen lasse.

Aber heute habe ich endlich mal ein wenig Zeit übrig, um ein wenig was zu schreiben. Diverse kleine Dinge, die so in letzter Zeit aufgekommen sind.

### Feuerwerk macht Bumm 🎆
Es ist Saison! Die meisten verbinden zwar Silvester und somit Winter mit Feuerwerk, aber wenn man kurz erwähnt, dass bei Sportveranstaltungen, Hochzeiten, Abifeiern, ~~Kirmes?~~ ~~Kirmesen?~~ ~~Kirmä?~~
~~Kirmi?~~ ~~Kirmasse?~~ [Kirmessen](https://de.wiktionary.org/wiki/Kirmessen) oft Feuerwerk abgebrannt wird, dann leuchtet jedem ein: Feuerwerk gehört in den Sommer!

Natürlich muss irgendwer die Arbeit machen. Zwar kann quasi jeder beantragen, für Feierlichkeiten ein Feuerwerk machen zu dürfen, jedoch geben Ordnungsämter immer seltener das Okay für Privatpersonen,
wahrscheinlich weil schon zu oft irgendetwas schief gegangen ist. Glücklicherweise gibt es da so [Dienstleister](http://www.sprengkraft.de/), die das übernehmen können und da arbeite ich nebenbei. Es
ist ein toller und interessanter Beruf mit immer wieder neue Herausforderungen. Aber es ist eben auch Arbeit. Und nebenbei hab ich ja auch immer noch meinen ganz normalen 40 Stunden Job. Das schlaucht
also im Moment schon ganz ordentlich. Es ist ein wunderbarer Kontrast zu meinem Hauptberuf "Bürostuhlsitzer", aber meine Freizeit geht halt schon ordentlich runter in der Zeit, wodurch natürlich auch
das ein oder andere Projekt auf der Strecke bleibt.

Hier mal paar Bilder vom Kirmesfeuerwerk in Geldern. Das Gebäude im Hintergrund beim dritten Bild ist meine alte Schule 😉.

https://www.instagram.com/p/BjF7eq6AZqd/?taken-by=nozomibk

Für Feuerwerkliebhaber haben wir übrigens auch eine [Gruppe bei Facebook](https://www.facebook.com/groups/205969660037871/).

### Zeitumstellung
Die EU macht derzeit [eine Umfrage zur Zeitumstellung](https://ec.europa.eu/eusurvey/runner/2018-summertime-arrangements?surveylanguage=DE)! Endlich überlegt man sich, den Wahnsinn wieder abzuschaffen.
Ursprünglich war der Hintergedanke ja, dass man dann im Sommer Strom sparen würde. Leider klappt das nicht ganz so gut, wie erwartet. Die negativen Folgen für Menschen sind hingegen ein echtes Problem,
da kann ich selber ein Lied von singen. Klar, nicht jeder hat Probleme mit der Zeitumstellung. Manche sind nach zwei Tagen wieder komplett im Rhytmus. Aber es sind eben nicht alle. Es gibt reichlich Leute,
die unnötig dadurch belastet werden, die sowieso schon Schlafprobleme oder genug anderen Stress haben, bei denen es Wochen dauert bis die Umstellung verarbeitet ist.

Daher einfach mal für die Abstimmung der Sommerzeit aussprechen!

### Ich hab nen Herd! Mit Ofen!!
Was ein unglaublich unspektakuläres Thema, aber auch die gibt es. Ich habe drei einhalb Jahre hier in der Wohnung gewohnt, ohne einen richtigen Herd. Ich hatte eine alte Herdplatte, ich glaub aus dem
letzen Jahrtausend, die mein Vater damals auf einen 230V Stecker umgebaut hat. Wunderbar als Ersatz oder wenn man für Feiern mal zusätzliche Platten brauchte. Statt Ofen hab ich meine Pizza in der Mikrowelle
mit Backofenfunktion gemacht, aber mehr ging darin nie. Jetzt hat vor paar Wochen die Platte gezeigt, dass das Alter an ihr nagt: Wenn man zwei Platten auf voller Kraft laufen lässt, fängt sie an zu qualmen.
Daher habe ich mir endlich einen Herd mit Ofen gekauft! Beim Ausschnitt für die Herdplatte musste ich aufgeben - meine Stichsäge ist eigentlich nur strombetriebener Müll. Aber es ist auch besser so, weil
die Platte ist für den Umbauschrank grenzwertig groß und ich hätte den ganzen Aufbau wahrscheinlich nur kaputt gemacht. Glücklicherweise ist mein Vater Schreinermeisten und er hat mir die Arbeitsplatte
zurecht gesägt, sodass ich den ganzen Kram einbauen konnte. Für viele ist es banal, weil zu normal, aber ich freu mich, dass ich endlich wieder einen _richtigen_ Herd mit _richtigem_ Ofen habe!

https://www.instagram.com/p/Bk8XI6Ggwcx/?taken-by=nozomibk

### Ich hatte Geburtstag 🎉
Ja, auch das ist passiert, ich hatte am 2.7. ein Level Up. Ich mach darum nicht viel Lärm, aber wenn ich schon blogge, dann sollte das wenigstens Erwähnung finden. Immerhin hab ich mir zu meinem Geburtstag
seit langem mal wieder richtig leckeres Sushi gegönnt!

https://www.instagram.com/p/BkvTd8PgzqM/?taken-by=nozomibk

Am Samstag (nachdem mein Vater die Arbeitsplatte fertig gemacht hat) gab es dann noch mit meiner Family lecker Burger essen gehen. Zudem hab ich jetzt nen paar Pflanzen bekommen, die ich raus auf die
Fensterbank stellen und das Jahr über vergessen kann. Ja, ich wollte die Pflanzen wirklich haben, mit Betonung auf Bienenfreundlich 🐝. Ja, ich bin ein verkappter Öko, der [Bienen retten](https://savethebeesproject.com/)
will.

![]({{ site.baseurl }}images/posts/2018-07-08_pflanzen.jpg)

Aber ich brauch da erstmal nen zweiten Blumenkasten und neue Blumenerde, bevor ich die Welt retten kann.

### Projekte
Wie immer hab ich nen Haufen an Projekten im Kopf und auf der Warteliste oder sonstwie vernachlässigt. Bestes Beispiel: Dieser Blog und mein Twitch-Account. Aber hier einfach mal so nen bissle was, was
in meinem Kopf rumgeistert oder aktuell bei mir von relevanz ist.

- [Mapban.eu](https://mapban.eu/): Ein kleiner Dienst, vor allem für Caster im Rainbow Six Siege Bereich. Ich bin da als unterstützende Kraft tätig, beziehungsweise momentan eher "Wächter der Ordnung".
Der gute Sascha hat das Projekt ins Leben gerufen, aber wenig Programmiererfahrung. Dadurch ist ein großer Haufen schwer zu verbessernder Code (oder Kot?😝) entstanden. Die ersten Aufräumarbeiten sind
erledigt und nun beschränke ich mich wegen Zeitmangel darauf, seine Arbeit zu kontrollieren. Aber damit lernt er auch am meisten und die letzten Sachen brauchte ich nichtmal zu beanstanden 👍.

- Shadowrun: Ich spiel jeden Mittwoch mit paar Freunden Shadowrun. Momentan hab ich nen paar echt gute Ideen für nen Run, die ich ausarbeiten muss, bin sehr gespannt wie gut der klappt. Zudem will ich
eigentlich sau gerne mit technischen Spielereien arbeiten. Einen Laptop mit WLAN-AP zum Beispiel, auf dem die Spieler mit ihrem Handy dann Dateien einsehen können, wie Baupläne und so weiter. Oder mit
einem Beamer den aktuellen Gebäudeplan auf den Tisch projezieren, sodass jeder genau ansagen kann wo er hingeht und jeder sieht wo wer gerade steht, wie weit es zur nächsten Deckung ist etc. Leider wird
das wohl daran scheitern, dass wir zwei eher altmodisch eingestellte Rollenspieler mit dabei haben.

- DJing: Wer mich kennt, weiß ich mag Musik mixen. Vor ca. 15 Jahren hatte ich bereits Turntables und Mixer. Hab damals aber alles auf- und weggegeben, weil als Schüler die Kosten für neue Platten auf
Dauer nicht zu stemmen waren. Seit anderthalb Jahren hab ich nun nen Controller, mit dem ich auch paar mal im Stream aufgelegt habe, aber derzeit staubt er voll. Eigentlich will ich gerne mal wieder damit
loslegen und vor allem in Richtung Industrial und EBM gehen, beziehungsweise allgemein "düstere" elektronische Musik. Irgendwann klappt das bestimmt.

- Twitch: Ja, auch da juckt es mich in den Fingern. Allerdings muss ich mein komplettes Setup neu machen, die ganzen OBS-Szenen und Overlays tun es nicht mehr. Aber weil das etwas mehr Zeit kostet, steht
das erstmal hinten an.

### Twitch, warum?
Zum Abschluss will ich noch ein wenig über Twitch meckern. Und es geht gar nicht mal um mich, sondern um einen befreundeten Streamer. Ich kenne keinen, der sich so viel Mühe für seinen Stream macht, mit
so viel Liebe seine Community aufbaut und richtig ordentliche Konzepte für seine Sendung aufbaut. Wie viele große Streamer hab ich gesehen, die kaufen sich nen Overlay und machen nichts, außer zu zocken.
Egal wie schlecht ich gelaunt bin, sobald er streamt gehts mir gut. Die ganze Woche kann noch so scheiße und anstrengend sein, wenn am Freitag Back to Oldschool kommt, ist Entspannung angesagt. Und da
bin ich nicht allein, selbst wenn nur im Stream ein wenig gelabert wird anstatt zu zocken, fällt der Zuschauer-Counter nicht unter 20.

Aber was macht Twitch? Lehnt ihn als Partner ab. Was zur Hölle läuft falsch bei denen? Irgendwelche großmäuligen Vollidioten werden verpartnert, aber jemand mit Konzept und Niveau wird zappeln gelassen.

Aber genug gemeckert. Der Weg mag zwar schwer sein, aber das mit der Partnerschaft wird noch was! Also ganz dicker Shoutout an [Obake TV](https://www.twitch.tv/obake_tv), der geilste und sympathischste
Streamer den ich kenne!

https://www.instagram.com/p/BkBOgCKFkIS/?taken-by=kidsunekyuubi

Damit hätte ich dann so ziemlich alles vom Herzen geplappert, was in letzter Zeit angefallen ist. Bei Gelegenheit mach ich noch nen Anime-Feedback zur letzten Season, wird auch so langsam Zeit.