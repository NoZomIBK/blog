---
layout:      post
title:       "Ich hab doch keine Ahnung von Rubinen"
date:        2018-04-07T15:53+0200
author:      nozo
tags:        Entwicklung Programmieren Ruby Blog Rant
image:       images/posts/rage.jpg
description: Mach einen Blog haben sie gesagt. Macht Spaß haben sie gesagt. Jetzt fummel ich an Ruby Plugins ohne Ruby zu können.
---
Gestern habe ich meinen Blog um nen paar netter Plugins erweitert. Unter anderem [Lazy Tweet Embedding](https://github.com/takuti/jekyll-lazy-tweet-embedding), wo der Tweet automatisch embedded wird,
wenn ich den Link zu dem Tweet in eine einzelne Zeile schreibe:

[https://twitter.com/NoZomIBK/status/981503612953260032](https://twitter.com/NoZomIBK/status/981503612953260032)

wird also zu
https://twitter.com/NoZomIBK/status/981503612953260032

Soweit schön und gut. Da ich aber auch gerne mal Fotos auf Instagram packe, will ich die gleiche Funktionalität haben. Also Plugin kopiert, alle Tweet Klassen umbenannt, den Such-regex angepasst und...
kaputt.

Kann natürlich nicht sein, dass da irgendwas am Code jetzt kaputt ist, ich hab zwar keine Ahnung von Ruby, aber Klassennamen ersetzen kann ich dann doch schon. Also habe ich einen Fehler in meinem Regex
gesucht:

{% highlight ruby %}
def convert(line)
  r = /^https?:\/\/www\.instagram\.com\/p\/([a-zA-Z0-9_]+)\/[\S]*$/
  r =~ line ? get_html($~[1]) : line
end
{% endhighlight %}

Wer sich mit Regulären Ausdrücken auskennt sieht: der ist gut so. Letztlich liegt das Problem an einer ganz anderen Stelle: Das Plugin überschreibt eine Klasse, damit beim Konvertieren auch nach den Tweets
gesucht wird:

{% highlight ruby %}
module Converters
  class Markdown < Converter
    alias_method :parser_converter, :convert

    def convert(content)
      parser_converter(Jekyll::LazyTweetEmbedding.new.embed(content))
    end
  end
end
{% endhighlight %}

Das habe ich natürlich auch 1:1 kopiert und dann in `def convert(content)` das `LazyTweetEmbedding` mit `LazyInstagramEmbedding` ausgetauscht. Nur dass jetzt wohl zwei mal die Klasse `Jekyll::Converters::Markdown`
vorhanden war. Die kryptische Fehlermeldung hat mit bei der Erkenntnis aber nicht geholfen.

Aber einfach den Code in meiner Kopie auskommentiert und den Aufruf zu meinem Embedder in die andere Klasse kopiert.

Variante 1:
{% highlight ruby %}
def convert(content)
  parser_converter(Jekyll::LazyTweetEmbedding.new.embed(content))
  parser_converter(Jekyll::LazyInstagramEmbedding.new.embed(content))
end
{% endhighlight %}

Es hat jedoch nur den Instagram-Link umgewandelt. Also mal ein wenig probiert:

Variante 2:
{% highlight ruby %}
def convert(content)
  content = parser_converter(Jekyll::LazyTweetEmbedding.new.embed(content))
  parser_converter(Jekyll::LazyInstagramEmbedding.new.embed(content))
end
{% endhighlight %}

Variante 3:

{% highlight ruby %}
def convert(content)
  parser_converter(Jekyll::LazyTweetEmbedding.new.embed(Jekyll::LazyInstagramEmbedding.new.embed(content)))
end
{% endhighlight %}

Beide letzteren Varianten sorgrten nun dafür, dass der Tweet zwar eingefügt wurde, Instagram aber dafür ignoriert.

Also sehe ich mich schon vor der nächsten Aufgabe für diesen Blog: Ruby verstehen. Und warum darüber nicht gleich nen Blogpost verfassen, wozu ist denn der Blog ansonsten gut? :D

Ran an den Feind und Blogpost verfasst. Schön mit den Codesnipets und allem... nochmal gucken, was das Ergebnis bei Variante 1 war und zack: Es funktioniert wie gewünscht.

<div class="myvideo">
   <video  style="display:block; width:100%; height:auto;" autoplay controls loop="loop">
       <source src="{{ site.baseurl }}images/wtf.mp4" type="video/mp4" />
   </video>
</div>
<br />

Und ich habe definitiv nichts anders gemacht, weil ich einfach die History der Datei zurück gegangen bin. Ganz ehrlich, wer soll denn so bitte arbeiten?! Aber so schnell endet dann auch meine Reise in
die Gefilde von Ruby. Vorerst. Also kann ich immer noch sagen "Ich hab doch keine Ahnung von Rubinen"

PS: Als Beweis, dass Twitter und Instagram zusammen gehen, hier noch nen embedded Instagram-Bild:

https://www.instagram.com/p/BXFWOfJgwFb/?taken-by=nozomibk