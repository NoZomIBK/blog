---
layout: post
title:  "Gurkensuppe"
date:   2018-08-10T22:39+0200
author: nozo
tags:   Rezepte Kochen Küche
image:  images/posts/cooking.jpg
---
Ich hab das Rezept von einem Freund bekommen und weil ich es irgendwo festhalten will, warum nicht einfach eine Rezepte-Kategorie?

## Zutaten
- Vier Gurken
- Butter (ca 1,5 Esslöffel)
- Currypulver
- Hühner- oder Gemüsebrühe
- Ein Becher Sahne
- Creme Fraiche

## Zubereitung
Die Gurken schälen und entkernen. Anschließend in mundgerechte Stücke schneiden. Mit Curry und bei Lust ein wenig Zucker würzen und in der Butter angehen lassen. Ruhig genug Curry, die Gurken gleichen
viel aus.

Mit der Brühe auffüllen und zum kochen bringen. Nach paar Minuten ein paar Gurkenstücke rausfischen und zur Seite legen, damit man auch ein wenig was festes in der Suppe hat, den Rest mit einem Pürierstab
kleinmachen, anschließend können die Gurkenstücke wieder rein.

Zum Schluss den Becher Sahne und einen Kleks Creme Fraiche hinzufügen und verrühren.

Dazu soll geräucherter Lachs sehr gut passen.

## Mein Fazit
Das Rezept wirkt echt simpel, aber die Gurken machen mehr Arbeit als man denkt. Leider trifft es absolut nicht meinen Geschmack. Wenn es nichts anderes gibt, dann kann man es essen, aber wenn ich die 
Wahl hab, würde ich eher was anderes nehmen.

👨‍🍳👨‍🍳 Mehr Aufwand als erwartet  
🍳 Nicht mein Fall