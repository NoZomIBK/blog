---
layout: post
title:  "Interstellar"
date:   2018-11-10T12:31+0200
author: nozo
tags:   Review Physik Film
image:  images/posts/2018-11-10-interstellar.png
---
Nachdem mein [lieblings Wissenschaftspodcast](http://minkorrekt.de) in der für mich aktuellen [Folge](http://minkorrekt.de/methodisch-inkorrekt-folge-38-fazit-fart/) anfangen wollte zu spoilern, habe 
ich endlich mal Interstellar geguckt. Mein Review bleibt natürlich auch nicht spoilerfrei, aber ich glaube, so viele Leute gibt es nicht mehr, die den Film noch nicht gesehen haben.

Also erstmal zum physikalischen Aspekt, eigentlich hätten die Jungs von MInkorrekt da mehr zu sagen können, finde ich. Die Grundaussage "die Physik in dem Film ist komplett fürn Arsch" kann ich 
persönlich als nicht Physiker nicht nachvollziehen. Klar, dass man in ein Schwarzes Loch fliegt und das Raumschiff das unbeschadet übersteht - Hollywood, auch wenn sie versucht haben es mit "Das ist 
nur ein schwaches Schwarzes Loch, wenn man schnell genug ist, kann man vielleicht den Ereignishorizont ungeschadet überwinden". Aber ansonsten? Was mich vor allem interessiert: Ist es auch nur wieder 
Filmlogik, dass man Gravitationsanomalien durch die Zeit schicken könnte, oder ist das tatsächlich irgendwie eine echte Theorie? Der ganze Film baut genau darauf auf und ich wüsste gerne, ob da 
tatsächlich etwas dran sein könnte.  
Was ich auch nicht verstehe und was nicht erklärt wurde: Wieso funktioniert Kommunikation nur in eine Richtung durch das Wurmloch, aber die Reise offensichtlich in beide Richtungen? Klar, es erhöht 
wunderbar das Drama, aber gibt es dazu vielleicht eine logische Erklärung?  
Was mich echt an dem Film gefreut hat: Sie haben die coolen, bombastischen Geräusche im Weltall weggelassen.

Wo ich tatsächlich die Kritik von Reinhard Remfort im Podcast angehen muss: Hat er die letzte halbe Stunde des Films verpasst? Er meckert darüber, dass Cooper ausgerechnet die Koordinaten der geheimen
NASA Basis im Zimmer seiner Tochter binär kodiert findet. Ja, am Anfang des Films ist es natürlich eine "Yeah, Hollywood" Situation, aber am Ende wird es ja aufgeschlüsselt und ist meiner Meinung nach
auch nicht direkt sinnlos, sondern im Grunde genau der Dreh- und Angelpunkt der gesamten Story: Kommunikation über Gravitation durch die Zeit.  
Auch der Punkt, dass Cooper a) Farmer, b) Ingenieur, c) Pilot und d) Physiker ist. Das ein Physiker zum Ingenieur wird ist nun nicht super abwegig. Und das ein Pilot, der dafür ausgebildet wird ins 
All zu fliegen, nicht nur Pilot sondern irgendwie auch Wissenschaftler/Ingenieur ist, sollte nicht so überraschen. Dass diese Person dann - nachdem die Welt zusammengebrochen ist und eine globale 
Hungersnot herrscht - zu einem Farmer wird, um seine letztlich Familie zu ernähren, da ist auch für mich kein Problem zu erkennen. Ja, es ist eine begabte Person, aber das sind die meisten Raumfahrer.
Man könnte sagen, es ist eine Grundvorraussetzung.  
Und auch die Kritik am Schleudersitz. Ich habe das Gefühl, die Herren haben während des Kinobesuchs bereits disskutiert anstatt den Film zu sehen. Das Raumschiff war auch für den Flug in der 
Atmosphäre gedacht, es war das gleiche Modell, mit dem die Leute zwischen Planet und Ringmodul geflogen sind. Bevor man also auf einen Planeten kracht, möchte man vielleicht lieber den Schleudersitz 
nutzen. Und warum Cooper den Schleudersitz irgendwo im Schwarzen Loch gezündet hat: Er war im Delirium und hat nicht viel mehr mitbekommen als die Warnung "EJECT" vom Bordcomputer. Und darauf ist man 
gegebenenfalls als Pilot trainiert: Man kriegt nichts mit, hört den Computer schreien: "Zieh den Hebel sonst bist du tot!", dann zieht man halt den Hebel.  
Gut, der Punkt "Quantendaten™" ist natürlich kompletter Unsinn und einfach nur ein Aufhänger, mit dem man begründet: Warum will Cooper in das Schwarze Loch und wieso ist Cooper der Held, der die 
Menschen auf der Erde rettet. Da hat es auch mir die Fußnägel aufgerollt.

Die Kritik am Schwarzen Loch und was darin passiert finde ich auch ein wenig überzogen. Der Kernplot ist ja, irgendetwas hat das Wurmloch geschaffen, die Dinger können nur künstlich geschaffen werden.
Irgendetwas kommuniziert mit uns über Gravitationsanomalien. Aber das Schwarze Loch soll ein normales Phänomen sein. Offensichtlich ist das Schwarze Loch ja auch künstlich, damit man die vier 
Dimensionen des Kinderzimmers in drei Dimensionen darstellen kann, damit Cooper kommunizieren kann. Wenn dann das Schwarze Loch Cooper töten würde beim reinfliegen, wäre das verdammt kontraproduktiv 
für die Erschaffer des Schwarzen Lochs - deren kompletter Plan wäre hinüber. Und außerhalb der Filmwelt gesprochen: Meines Wissens sind wir in Bezug auf Schwarze Löcher ziemlich beschränkt. Es gibt 
Theorien dazu, aber man kann wenig beweisen und vor allem beobachten. Wieso ist dieser Punkt "Durchfliegen und dahinter in einem seltsamen Raum-Zeit-Konstrukt hängen" so abwegig? Es ist ein wenig wie 
die Frage "Was war vor dem Urknall?" - man kann sich vieles ausdenken, aber bisher haben wir keinen Anhaltspunkt, um irgendwelche echten Aussagen zu treffen.

Ein kleinen Punkt, den ich persönlich zur Technik des Films habe: Die Zeitsprünge fand ich cool und zugleich störend. Es gibt eine Hand voll Situationen, wo mit einem Cut plötzlich Jahre 
vergangen sind, ohne dass es dem Zuschauer direkt verdeutlicht wird und mich immer kurz hat nachdenken lassen "Wie viel Zeit ist gerade vergangen?". Klar kann es an rausgeschnittenen Szenen gelegen 
haben, der Film war ja bereits so lang genug, aber ich glaube es war bewusst so gemacht, weil Zeit ja auch als kostbare Ressource beschrieben wird. Daher war es ein toller Effekt, als beispielsweise 
der Sling-Shot um den Mars gerade stattgefunden hat und plötzlich ist es zwei Jahre später und man steuert das Wurmloch an. Andererseits ist das eher der Gedanke im nachhinein und während des Films 
selber hat es mich kurzzeitig verwirrt.

Und zum Abschluss noch zu den nicht physikalischen Aspekten. Liebe überwindet alle Hürden, aber die Raum-Zeit? Was für eine gequirrlte Scheiße ist das denn bitte? "Vielleicht ist die Liebe zu Edmunds 
nicht nur eine Emotion, sondern tatsächlich eine Verbindung", "Die Liebe zu meiner Tochter erzeugt diesen Tesserakt, mit dem ich zu ihr kommunizieren kann", Wer ist auf so einen Unsinn gekommen? Hat 
irgendwer die Film-Checklist nach einer Stunde Film ausgegraben und gesehen, dass der Punkt "Philosophische und übergeordnete Rolle der Liebe" noch nicht abgehakt ist? Es ist absolut unnötig 
reingequetscht und wirkt auf mich eher störend, man hätte es weglassen sollen.  
Ebenfalls die Mutmaßung von Cooper, dass diese "Irgendwer", die für das Wurmloch und die generelle Rettung der Menschheit scheinbar verantwortlich sind, nicht irgendwelche Außerirdische sind, sondern 
die Menschen aus der Zukunft, die sich zu fünfdimensionalen Wesen weiterentwickelt haben und sich nun durch die Zeit bewegen können und daher die Menschen von der Erde retten.Nein, einfach nein! Diese
Mutmaßung ist ohne jegliche Begründung oder Grundlage. Es wird einfach in den Raum geschmissen. Und Cooper selber äußert es nichtmal als Mutmaßung, sondern quasi als Erkenntnis. Woher soll bitte diese
Erkenntnis kommen? Warum baut man so einen sinn- und haltlosen Schwachsinn ein?  
Und wo wir gerade bei sinn- und haltlosen Schwachsinn sind: Murph geht nochmal ihr Kinderzimmer durch und schaut sich alles aus der Kindheit an und es offenbart sich ihr, dass die ganzen Botschaften 
von ihrem Vater kommen. Dass der Geist der Kindheit nicht irgendein Wesen ist, sondern ihr Vater. Wieder einmal komplett ohne Fundament. Wegen ein paar binär und mit Morsecode übertragene Daten. Alles
über die Verbindung der Liebe offensichtlich. Wäre dieser Aspekt nicht gewesen, hätte ich den Film tatsächlich gemocht. Aber das hat den kompletten wissenschaftlichen Anstrich zerstört.