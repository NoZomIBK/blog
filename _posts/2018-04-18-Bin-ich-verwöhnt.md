---
layout: post
title:  "Bin ich verwöhnt?"
date:   2018-04-18T17:56+0200
author: nozo
tags:   Blog Alltäglich Philosophie
image:  images/posts/thinking.jpg
---
Also das man gut lebt sollte jedem klar sein, der das hier lesen kann. Aber heute musste ich durchaus feststellen, dass ich recht verwöhnt bin, oder nicht?

Fang ich mal mit dem Auslöser für diesen Post an und philosophiere dann weiter. Ich habe mir was Samstag nachmittags bei Amazon gegönnt - eine Switch und zwei Spiele - und mich kurz darauf geärgert. Nicht
darüber, wie teuer es war oder dass ich ein anderes Angebot hätte nehmen können. Nein, es ging um den Liefertermin der Switch: Mittwoch. Die Spiele wurden mir für Montag angekündigt, warum die Switch
nicht auch? Was läuft da schief, dass die Konsole so viel länger brauch. Aber naja, es ist Amazon, bestimmt kommt es dann spätestens Dienstag. Am Montag dann die Meldung bekommen, dass es versandt
sei - mit DPD?! Jetzt wurde es mir klar und ein genauerer Blick auf die Bestellung bestätigte es: Ich habe bei einen Händler bestellt anstatt bei Amazon und dann auch noch einer, der es nicht über Amazon
versenden lässt, sondern das noch selber macht. Und "Ihre Sendung wurde versandt" bedeutet in diesem Fall auch nur "Wir haben bei DPD bescheid gesagt und den Paketaufkleber gedruckt, irgendwann kommen
die das dann abholen". Und jetzt ärger ich mich darüber, dass ich von Amazon den günstigsten Preis bekommen habe, nicht den günstigsten Preis + schnellste Lieferung.

#### Hat mich Amazon zu sehr verwöhnt?
Ich bin schon lange ein Fan davon, bei Amazon damit rechnen zu können, dass die Bestellung meistens am nächsten Tag bereits eintrifft. In kritischen Fällen klappt das natürlich nie, aber ich glaube, das
ist eh ein anderes, kosmisches Gesetz. Die logistische Leistung von Amazon ist grandios - auch wenn es mit den Arbeitsbedingungen nicht so rosig aussieht. *Über die Arbeitsbedinungen will ich hier gar nicht
reden, das werde ich wahnscheinlich in nem anderen Post nachholen.* Bin ich bei Amazon nun davon eher enttäuscht, dass sie mir nicht die allerbeste Option rausgesucht haben beim bestellen? Oder einfach
nur, dass es so lange dauert? Das es überhaupt noch Anbieter gibt, bei denen es mehrere Tage dauert? Ist es ein Problem mit DPD oder vom Händler, dass es so lange dauert? Und vor allem: ist meine Enttäuschung
überzogen oder gerechtfertigt?

Wenn ich mal zurück denke, es ist nichtmal 15 Jahre, also hälfte meines Lebens her, dass man sich Sachen bestellt hat, das Geld überwies und dann wartete. Da hat man dann seine Sachen etwa eine Woche
später erhalten. Allein für den Versand musste man zwei Tage rechnen, egal ob DHL oder was auch immer. Wobei ich sagen muss, viele Logistikanbieter - auch DPD - kommen bei mir auch eher am zweiten Tag
erst an. Das ist auch ein Grund für meine Enttäuschung: Ich hatte so oft Probleme mit DPD und habe es auch von vielen anderen schon gehört, dass besonders DPD oftmals negativ auffällt - warum wählt man
echt noch DPD als Versandunternehmen? Auch preislich sind die nicht Platz 1.

Also wenn man so in der Vergangenheit schwelgt muss man sagen, dass es durchaus ein wenig übertrieben ist, sich wegen einer halben Woche zu ärgern. Aber wir befinden uns in eine Ära der Automatisierung
und Robotik. Ich bin nicht unzufrieden mit der Situation, aber ich weiß es geht besser, wieso macht man es also nicht besser? In China werden Wahren automatisch von Robotern aus dem Lager geholt und
verteilt. Bei Amazon laufen immer noch die Arbeiter durch die Gänge. Warum muss alles immernoch von Postboten gebracht werden, wo bleiben die Lieferdrohnen? Wenn man mit dem Status quo zufrieden bleibt
kommt man nie in der Zukunft an! Deswegen bin unzufrieden, ich will in die Zukunft. Das Jahr 2000 ist schon lang vorbei, das kann man nicht mehr als Maßstab nehmen!

#### Und wenn wir die ganze Sache weiter spinnen?
Ohne Visionen geht es nicht vorran, also wieso nicht mal ein wenig mehr rumspinnen? Wann geht es endlich mit Städten ohne Autos los? Ich wäre einer der ersten, der sein Auto am Stadtrand stehen lässt
und mit dem Bus rein fährt, wenn es nicht unglaublich unpraktisch wäre. Zug fahren säh ich auch nicht als Problem, wenn es so zuverlässig wie in Japan wäre. Und dann ist es auch noch deutlich billiger
mit dem Auto zu fahren - trotz Stau und Stadtverkehr - anstatt sich eine Dauerfahrkarte für die Bahn zu holen, wieso sollte ich das also tun, wenn es nur Nachteile für mich hat? In jedem guten Utopia
kann man ohne Bedenken in ne Bahn springen und dahin fahren, wo man hin will. Bei uns wird es bereits schwierig, wenn man Verbundübergreifend den besten Tarif suchen will.

Und auch andere Szenarien wären mit funktionierenden Drohnen ein Traum: Müllabfuhr läuft automatisiert in der Nacht. Man stellt die Tonnen raus und von alleine werden die nachts eingesammelt. Lieferungen
an die Geschäfte in der Innenstadt bekommen ihre Rollis in den frühen Morgenstunden vor dem Berufsverkehr, ohne dass dafür irgendwer auf Schlaf verzichten müsste. Aber was heißt schon "vor Berufsverkehr"
wenn dieser deutlich nachlässt wegen oben genannter Utopie.

#### Nicht nur Logistik
Auch in anderen Bereichen sehe ich Verwöhnpotential. Viele Services im Internet werden kostenlos angeboten. Zum Teil finanziert durch Werbung, ein anderes Konzept arbeitet mit dem Prinzip "Grundfunktionen
kostenlos, Extras kosten extra, Firmen sowieso". Oft hört man die Datenschützer rufen "Wenn der Dienst kostenlos ist, seid IHR das Produkt", besonders wenn es um Facebook geht. Aber stimmt das? Ist das
wirklich ein Problem? Das Konzept ist ja nicht, dass die Daten rausgegeben werden, sondern dass aufgrund der Daten die Werbung gezielter gezeigt wird. Also ist es nicht nur für die Werbung schaltenden
Firmen profitabel, auch für mich ist es interessanter über Produkte informiert zu werden, die mich wirklich interessieren, als über irgendwas meiner Meinung nach total sinnloses wie Barbie. Das Produkt
ist also nicht der User, sondern die Verbindung zwischen User und Firma. Aber da kann man natürlich auch lange drüber streiten und die Probleme, die mit Datenprofilen einher kommen sind nochmal eine ganz
andere Sache.

Aber auch die Dienste, wo Grundlegendes kostenlos angeboten wird sind interessant. Musik höre ich üblicherweise über Google Music - Da kann ich genug Musik für mich Hochladen, wer mehr will zahlt. Oder
man finanziert den Dienst, indem man bei Google Music im Shop die Musik einkauft. Für meine Software (auch dieser Blog) nutze ich GitLab. Für kleine Projekte ist es wunderbar, wenn man es größer oder
als Firma nutzen will und dafür mehr Resourcen benötigt darf zahlen. Auch bei fileee - wo ich arbeite - Kriegt man die Grundlage, die für eine Privatperson reichen sollte, kostenlos. Wer den Papierkram
seiner Ich-AG verwalten will zahlt. Große Firmen bekommen von uns eine einfache Schnittstelle um mit ihren Kunden zu kommunizieren, was für sie natürlich nicht kostenlos, aber lohnend ist. Ja, ich erwarte
heutzutage, dass die nötigen Funktionen für eine normale Privatperson kostenlos sind. Ich denke nicht, dass das verwöhnt ist. Man muss sich sein Geschäftskonzept passend aufbauen, um das kleine Angebot
kostenlos bereitstellen zu können, soetwas wird 2018 erwartet.

#### Unterm Strich
Ich bin nicht wirklich unzufrieden, aber die Grenzen des Mögliche sollte durchaus herausgefordert werden, damit in Zukunft das Unmögliche zu erreichen. Also immer Vorwärts in Richtung Utopia!