---
layout: post
title:  "Zwanghafter Sport, Motivation ist Mist"
date:   2018-04-10T22:29+0200
author: nozo
tags:   Sport Gesundheit Rant
image:  images/posts/2018-04-10-sport.jpg
---

Aus aktuellem Anlass (ich war im McFit) schreib ich heute mal über meine Sportlichkeit und Probleme diesbezüglich. Jeder der mich sieht weiß sofort: Ich bin in Form. Leider ähnelt die Form eher der einer
Kugel.

Das war nicht immer so. Generell war ich immer ganz gut im Sport und hab auch gerne Sport gemacht. Während meinem Dualstudium in Aachen war so das Maximum meines Sportlebens. Ich hab mit viel Freude
regelmäßig Parkour gemacht, auch wenn es mal weh tat oder man eine Woche Muskelkater davon getragen hat. Nen Sprung über eine Tischtennisplatte war kein Problem und auch eine 4 Meter Mauer kam ich hoch.

![Parkour in Aachen]({{ site.baseurl }}images/posts/2018-04-10-parkour1.jpg){:width="45%"}
![Parkour in Aachen]({{ site.baseurl }}images/posts/2018-04-10-parkour2.jpg){:width="45%"}

Dazu kam dann noch beim Hochschulsport Ju-Jutsu und ein wenig Taekwondo und Arnis, regelmäßiges Klettern im Tivoli-Rock und Bouldern in der frisch gebauten Boulderhalle (die es wohl scheinbar nicht mehr
gibt). Also man konnte durchaus sagen, ich hatte volles Programm und es hat mir mega Spaß gemacht.

Generell habe ich schon immer Spaß dabei gehabt, mich kreativ zu bewegen. Laufen, Radfahren, Gewichte stämmen war noch nie was für mich. Auch beim Sport wollte ich mein Hirn nutzen müssen und ich finde
auch, dass es deutlich eleganteres Training ist und man ein besseres Körpergefühl bekommt als aufn Fahrradsattel.

#### Was ist nur passiert?
Jetzt ist natürlich die große Frage an dieser Stelle: Wieso ist diese Zeit vorbei? Es begann damit, dass meine Ausbildung zu Ende war. Hochschulsport war nicht mehr, ich war ja kein Student mehr. Ich
habe zwar ansonsten weiterhin Sport gemacht, aber mich auch dran gewöhnt mir Schokolade kaufen zu können, wann ich will, das Geld war ja jetzt nicht mehr knapp. Im Winter war dann generell nur noch
Bouldern angesagt, Parkour im Winter ist nicht ganz so spaßig. Anschließend kam dann noch mein Umzug nach Münster. Ich kannte keine Sau zum Sport machen und hatte keine Boulderhalle mehr, die fünf Minuten
entfernt war, zu der man dann nach Lust und Laune gegangen ist. Dazu kam dann noch das neu angefangene Studium, womit Sport - abgesehen von zur-Uni-radeln - erstmal komplett wegfiel (und man erinnere
sich an die Schokolade, die ich mir immer kaufen konnte)! Die Fitness schlich davon, aber das war noch kein Problem.

Nachdem ich mich in Münster eingelebt und wieder Studentenstatus hatte, sah ich mir das Hochschulsportangebot an und siehe da: Parkour! Wie geil war das denn bitte? Parkour in ner Halle und so. Mit Matten
und Kästen und Stangen und allem. Beim ersten Training wurden meine Erwartungen noch übertroffen: Es gab sogar so lustigen Gymnastik-Trampolinboden - perfekt um mal ein wenig an Tricks und Flips zu üben.
Ich war motiviert, hatte Spaß und tobte mich aus. Nur doof, dass ich ein halbes Jahr lang nahezu nichts gemacht habe. Ergebnis war, dass ich mich beim dritten oder vierten Training übernommen habe. Zunächst
hab ich mir nicht viel dabei gedacht, dass ich Rückenschmerzen hab, sowas kommt halt vor. Aber die Beschwerden wurden nicht besser, wodurch ich kein Sport machen konnte. Meine Essgewohnheiten sind allerdings
geblieben - damit fing es dann an. Nach sage und schreibe zwei Jahren chronischen Beschwerden habe ich es doch endlich mal zustande gebracht, zum Arzt zu gehen. Diagnose: Bandscheibe Matsche.

#### Absturz
Auch wenn mein Arzt mir erklärt hat, dass mit Training das wieder in Ordnung zu bringen sei, ist meine Welt erstmal zusammen gebrochen. Immer wieder kam in mir das Horrorszenario hoch: Noch ein bisschen
und ich könnte Hüftabwärts gelähmt sein. Wenn die Beschwerden stärker wurden hatte ich bereits leichte Taubheit, also kann es nicht so weit weg sein - so meine Vorstellung. Leider bin ich von Grund auf
ein extrem fauler und schlechter Mensch. Trotz dieser Ängste konnte ich mich nicht motivieren, ausreichend Sport zu machen. Nur gerade so viel, dass die Beschwerden weg gingen. Sobald ich keine Probleme
mehr hatte, war es auch wieder vorbei mit Sport. Und weiterhin hab ich gut gegessen - ich konnte mir ja alles mögliche zu mampfen leisten - finanziell. Dann kommt auch noch dabei, dass ich bei jeglichen
negativen Gefühlen Appetit bekomme und Gute Laune durch vollstopfen erwirken wollte. Anfangs war es nicht so unglaublich schlimm, doch mein Körper hat es mir übel genommen. Leistungsabfall und immer öfter
Kopfschmerzen waren das Ergebnis. Meine typische Haltung zu Problemen ergab: Ist halt so. Eigentlich eine blöde Haltung. Es machte sich immer stärker bei der Arbeit bemerkbar, dass es einfach nicht mehr
so läuft wie früher. Folge davon war dann Angst, Stress, Druck, Selbstzweifel - reichlich Gründe, um sich wieder glücklich zu fressen. Man merkt, wo es hin geht. Irgendwann war ich dann wegen der andauernden
Kopfschmerzen beim Arzt und es stellt sich heraus, dass ich massiv zu hohen Blutdruck hatte. Das hies dann erstmal Tabletten schlucken, Ernährung umstellen und Sport. Mit den Tabletten lief es so einigermaßen
([Wenn ich eine Frau wäre, wäre ich oft Schwanger](https://www.youtube.com/watch?v=E-9rx2RXx1I){:target="_blank"}), Sport und Ernährung lief gar nicht. Durchgehender Stress auf der Arbeit, dazu dann eine
Trennung und viele sonstige Kleinigkeiten - es wurde gemampft wie nie zuvor gemampft wurde! Und Motivation zum Sport hielt durchschnittlich drei Tage. Die Kopfschmerzen jedoch entwickelten sich zu einem
dauerhaften Problem, ich hatte kaum einen Tag ohne Schmerzen - sie wurden zur Normalität.

#### Und nun?
Motivation zum Sport klappt einfach nicht. Ich habe verschiedenes ausprobiert, aber die typischen Selbstbetrügereien nach dem Motto "Nach dem Sport belohn ich mich mit X" hab ich nie durchgehalten. Stets
war der Gedanke dabei "Wieso sollte ich mich selbst verarschen" und der Schokoriegel, den es nur nach Sport geben sollte, wurde einfach so verdrückt. Bei der Ernährung ist es auch nicht viel besser. Ich
koche nicht gerne. Damit meine ich nicht "ich habe keinen Spaß beim kochen" sondern "kochen bereitet mir schlechte Laune". Es gibt sehr wenig Sachen, bei denen ich mich überwinde mich an die Herdplatten
zu stellen. Dazu bleibt das Problem auf Stress und Probleme mit Essen zu kontern. Meine beste Variante war jetzt lange Zeit, eine Tüte M&Ms im Kühlschrank zu haben, die haben meist ein bis zwei Wochen
gehalten. Alles andere habe ich irgendwie immer sehr schnell verdrückt. Aktuell versuche ich die M&Ms durch Trauben zu ersetzen, was aber durchaus kostspieliger ist und zudem gibt es zur Zeit nur Trauben
aus Südamerika, Afrika oder Indien. Da wo man eigentlich als Gutmensch keine Trauben her haben will, weil böse Ausbeutung und Transport rund um den Globus und so! Mal sehen, wie ich das gedreht bekomme.

https://www.instagram.com/p/Bgs636WAhUn/?taken-by=nozomibk

Und der Sport? Er läuft gerade mal wieder. Und zwar mit einer etwas anderen Methode als ich bisher immer probiert habe: Zwang. Mir selbst gut zu sprechen oder die ganzen Selbstmotivationstricks greifen
bei mir einfach nicht. All die Ratgeber "Wie überwinde ich meinen inneren Schweinehund" die darauf hinauslaufen: Einfach mal anfangen und dann wird das schon - unnütz gegen meine Faulheit. Feste Tage
raussuchen und einen festen Plan machen: Ne, wird nichts. Fällt es einmal aus wegen Krankheit (und mit chronischen Kopfschmerzen kann man schnell mal keinen Sport machen) und schon bröselt die Struktur
und man lässt es immer öfter sausen. Was am ehesten noch gehen würde, wäre jemanden zum gemeinsam Sport machen haben. Da kommt nur das Problem, dass ich hier in Bochum *keine* Sau kenne und verdammt
Kontaktscheu bin. Nachdem meine Kopfschmerzen das letzte Mal jedoch wieder so richtig aufgedreht haben und mein Blutdruck eine Woche bei 180 zu 120 stand, habe ich einen Entschluss gefasst: Ich werde
Sport nicht freiwillig machen. Freiwillig bedeutet, dass ich aufhören kann, wenn ich keine Lust drauf habe. Nein, Sport wird bei mir Pflicht. Selbst wenn ich kein Bock habe, darf ich es nicht ausfallen
lassen. Wenn ich krank bin, dann ist das so, aber sobald ich wieder fit bin steht auch Sport wieder auf dem Plan. Ich muss nicht lernen, mich zu motivieren, ich muss lernen mich zu zwingen. Und bisher
läuft das erstaunlich gut. Der einzig blöde Nebeneffekt: Die zwei Tage nach dem Sport sackt mein Blutdruck so weit ab, dass ich kaum etwas geschafft bekomme, dann steigt er wieder so weit an, dass mir
der Schädel platzt. Aber da muss ich durch, bis sich mein Körper an niedrigeren Blutdruck gewöhnt und es mir allgemein wieder besser geht.

Ob ich je wieder Parkour machen kann bezweifel ich, auch wenn es mir bei jedem Geländer und Mäuerchen in den Fingern juckt. Aber mindestens Klettern will ich wieder können!