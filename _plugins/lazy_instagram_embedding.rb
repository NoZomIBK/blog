require "open-uri"
require "json"

module Jekyll

  # convert instagram url to embedding html
  class LazyInstagramEmbedding
    def get_html(id)
        url = "https://api.instagram.com/oembed?url=http://instagr.am/p/#{id}/"
        JSON.parse(open(url).read, { :symbolize_names => true })[:html]
    end

    def convert(line)
    line = line.gsub(/\r\n?/, "\n")
      r = /^https?:\/\/www\.instagram\.com\/p\/([a-zA-Z0-9_]+)\/[\S]*$/
      r =~ line ? get_html($~[1]) : line
    end

    def embed(content)
      content.lines.collect {|line| convert(line) }.join
    end
  end

  # for html, extend converter as a plugin
  class EmbeddingInstagramIntoHTML < Converter
    safe true
    priority :low

    def matches(ext)
      ext =~ /^\.html$/i
    end

    def output_ext(ext)
      ".html"
    end

    def convert(content)
      Jekyll::LazyInstagramEmbedding.new.embed(content)
    end
  end

end