#!/usr/bin/env bash

images=$(ls images/posts/)
comics=$(ls images/comics/)
mkdir -p public/images/posts/comics

sizes=("320x180" "400x225" "752x423")

resize(){
    size=$1
    input=$2
    output=$3
    if [[ -f "public/images/posts/$size/$output" ]]
    then
        return
    fi
    cmd="convert $input -resize $size public/images/posts/$size/$output"
    echo ${cmd}
    ${cmd}
}

for size in ${sizes[@]}
do
    mkdir -p public/images/posts/${size}/comics
done

for image in ${images}
do
    for size in ${sizes[@]}
    do
        resize ${size} "images/posts/${image}" ${image}
    done
done

for image in ${comics}
do
    if [[ ! -f "public/images/posts/comics/$image" ]]
    then
        cmd="convert images/comics/$image -crop 1216x684+0+0 +repage public/images/posts/comics/$image"
        echo ${cmd}
        ${cmd}
    fi

    for size in ${sizes[@]}
    do
        resize ${size} "public/images/posts/comics/${image}" "comics/${image}"
    done
done
