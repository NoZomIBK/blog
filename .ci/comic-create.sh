#!/usr/bin/env bash

COMIC_FOLDER="images/comics"
TARGET_FOLDER="_posts"

rm -rf ${TARGET_FOLDER}/*

for comic in $(ls ${COMIC_FOLDER})
do
    file=${COMIC_FOLDER}/${comic}
    base=${comic::-4}
    release=$(git log --reverse --format="%aI" -- ${file} | head -1)
    target=${TARGET_FOLDER}/${base}.md

    if [[ -e ${target} ]]
    then
        continue
    fi

    title=$(echo ${base:11} | sed -e 's/[_-]/ /g')

    cat << EOF > ${target}
---
layout: comic
title:  "${title}"
date:   ${release}
author: nozo
tags:   Comic
comic:  ${file}
image:  images/posts/comics/${base}.png
---
EOF

    echo "created post ${target}"
done