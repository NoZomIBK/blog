#!/usr/bin/env bash
if [[ ! -f "$1.webp" ]]
then
    cmd="convert $1.$2 $1.webp"
    echo ${cmd}
    ${cmd}
fi