#!/usr/bin/env bash

find public/images -type f -exec file {} \; | awk '$3 == "image" { print $1"\n"; }' | perl -n -e'/(.+)\.([a-z]+).*/ && print "$1 $2\n"' | awk '\
{\
system(".ci/single-webp.sh "$1" "$2);\
}'